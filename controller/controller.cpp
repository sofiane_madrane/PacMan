#include "controller.h"

Controller::Controller(shared_ptr<Game> model)
{
    game_ = model;
}

void Controller::pacmanMove(Direction dir)
{
    game_->movePacman(dir);
}

void Controller::passNextLevel()
{
    game_->nextLevel();
}

void Controller::restart()
{
    game_->restartGame();
}

void Controller::oneMorePacman()
{
    game_->getMapGame()->reInitPacman();
    game_->getPlayer()->setAlive(true);
    game_->getMapGame()->reInitAllGhosts();
    game_->getPlayer()->setDirection(Direction::LEFT);
    game_->getPlayer()->setNextDir(Direction::LEFT);
}
