#ifndef CONTROLLER_H
#define CONTROLLER_H
/*!
 *\file controller/controller.h
 *
 */
#include "view/launcher.h"

/*!
 * \class Controller
 * \brief cette classe permet  de controler le jeu Pac-Man. Afin de pouvoir y interagir avec.
 *  Elle respecte  modèle de conception
 * "MVC".
 * \author Madrane, Taranto, Annu, Keenens, Ramirez
 * \version 0.1
 * \date 2017
 */

class Controller : public QObject
{
    Q_OBJECT
public:
    /**
     * @brief Controller Constructeur du controller du jeu Pac-Man
     * @param model Le jeu Pac-Man.
     */
    Controller(shared_ptr<Game> model);

private:
    /**
     * @brief game_ Le jeu Pac-Man.
     */
    shared_ptr<Game> game_;

public slots:
    /**
     * @brief pacmanMove Fonction qui modifie la direction du Pac-Man.
     * @param dir La direction souhaitée.
     */
    void pacmanMove(Direction dir);
    /**
     * @brief passNextLevel Fonction qui passe de niveau le modéle.
     */
    void passNextLevel();
    /**
     * @brief restart Redémarre le jeu, en notifiant le modéle.
     */
    void restart();
    /**
     * @brief oneMorePacman Notifie la perte de vie du Pac-Man.
     */
    void oneMorePacman();
};

#endif // CONTROLLER_H
