#ifndef PACMANTHREAD_H
#define PACMANTHREAD_H
/*!
 *\file model/game.h
 *
 */
#include "game.h"

/*!
 * \class PacManThread
 * \brief cette classe permet  de déplacer  le Pac-Man. C'est un thread indépendant au jeu.
 * Elle notifie le jeu(model), afin qu'elle notifie la vue pour un traitement spécifique.
 * \author Madrane, Taranto, Annu, Keenens, Ordonnez
 * \version 0.1
 * \date 2017
 */


class Game;


class PacManThread : public QThread // Elle hérite de Qthread
{
    Q_OBJECT
public:
    /**
     * @brief PacManThread Construis un nouveau thread Pac-Man, pour qu'il puisse se déplacer.
     * @param parent
     */
    PacManThread(QObject* parent = 0); // une réference de la game juste // pour pouvoir appeler les méthodes
    /**
      * @brief Détructeur d'un PacmanThread.
      */
    ~PacManThread();
    /**
     * @brief run Point de départ pour le thread.
     */
    void run();
private:
    /**
      * @brief game_ Représente le jeu.
      */
    Game * game_{nullptr};


signals:
    /**
     * @brief emitFruitTimer Signal notifiant l'ajout d'un fruit.
     */
    void emitFruitTimer();
    /**
     * @brief emitNotify Signal notifiant de notifier un changement à la vue.
     */
    void emitNotify();
    /**
     * @brief emitBeastModeTimer Signal notifiant le mode "manger les fantômes".
     */
    void emitBeastModeTimer();
};

#endif // PACMANTHREAD_H
