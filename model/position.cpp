#include "position.h"

Position::Position(){

}

Position::Position(float x, float y)
{
    this->x = x ;
    this->y = y ;
}

Position::Position(const Position &pos)
{
    this->x = pos.getX();
    this->y = pos.getY();
}

float Position::getX() const
{
    return x;
}

void Position::setX(float value)
{
    this->x = value;
}

float Position::getY() const
{
    return y;
}

void Position::setY(float value)
{
   this->y = value;
}

void Position::left()
{
    this->x-=5;
}

void Position::right()
{
    this->x+=5;
}

void Position::up()
{
    this->y-=5;
}

void Position::down()
{
    this->y+=5;
}

bool operator==(const Position &lhs, const Position &rhs)
{
    return ( (lhs.x == rhs.x) && (lhs.y==rhs.y) );
}

bool operator!=(const Position &lhs, const Position &rhs)
{
   return !( lhs == rhs );
}
