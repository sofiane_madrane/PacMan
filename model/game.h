#ifndef GAME_H
#define GAME_H
#include <list>
/*!
 *\file model/game.h
 */
#include <map>
#include <QFont>
#include <QGraphicsItem>
#include <QFontDatabase>
#include <QMultimedia>
#include <QMediaPlayer>
#include "mapgame.h"
#include "direction.h"
#include "o_sdo/observateur.h"
#include "o_sdo/sujetdobservation.h"
#include "pacmanthread.h"
#include "ghoststhread.h"

/*!
 * \class Game
 * \brief cette classe permet  de gérer le jeu Pac-Man.
 *  Elle gére toutes les caractéristiques et détails du jeu.
 *  Elle respecte le modèle de conception
 * "Observateur / SujetDObservation".
 *  Elle est sujet d'observation.
 * \author Madrane, Taranto, Annu, Keenens, Ordonnez
 * \version 0.1
 * \date 2017
 */


class PacManThread;
class GhostsThread;

class Game :  public QObject, public SujetDObservation
{
    Q_OBJECT
    friend class PacManThread;//Pour pouvoir acceder au attributs
    friend class GhostsThread;                          // privé de la class Game oklm
public:
    /**
      * @brief Game Constructeur de la partie.
      */
     Game();
     /**
       @brief ~Game Destructeur de la partie.
       */
    ~Game();
     /**
     * @brief movePacman Fonction qui déplace Pac-Man suivant une direction.
     * @param dir La direction à suivre.
     */
    void movePacman(Direction dir);
    /**
     * @brief nextLevel Fonction qui passe au niveau suivant.
     */
    void nextLevel();
    /**
     * @brief maybeNextLevel Fonction qui vérifie l'état du passage de niveau.
     * @return Etat du passage de niveau.
     */
    bool maybeNextLevel();
    /**
     * @brief restartGame Fonction qui renouvelle la partie.
     */
    void restartGame();
    /**
     * @brief getMapGame Accesseur de la map du jeu.
     * @return La map du jeu.
     */
    MapGame* getMapGame();
    /**
     * @brief getpacFont Accesseur de la police d'écriture du jeu originel.
     * @return  La police d'écriture.
     */
    QFont getpacFont();
    /**
     * @brief getLevel Accesseur du niveau de la partie.
     * @return  Niveau de la partie.
     */
    int getLevel();
    /**
     * @brief getBegin Accesseur du status du lecteur de média.
     * Lance le status de son.
     * @return Status du lecteur de media.
     */
    int getBegin();
    /**
     * @brief getPlayer Acceseur du joueur.
     * @return Joueur.
     */
    shared_ptr<Pacman> getPlayer() const;
    /**
     * @brief getGhosts Accesseur du vecteur de fantôme.
     * @return Vecteur de fantôme.
     */
    vector<shared_ptr<Ghost> > getGhosts() const;

public slots:
    /**
     * @brief resetFruit Fonction qui reinitialise l'emplacement des fruits
     * sur la map.
     */
    void resetFruit();
    /*Reset PacMan
     * FearMode des ghosts*/
    /**
     * @brief resetBeastMode Fonction qui reinitialise le beastMode de Pac-Man.
     */
    void resetBeastMode();
    /**
     * @brief notify Fonction qui notifie à l'observateur de rafraichir la vue.
     */
    void notify();
    /**
     * @brief startFruitsTimer  Fonction qui démarre le timer d'ajout du fruit.
     */
    void startFruitsTimer();
    /**
     * @brief startBeastMode Fonction qui démarre le timer le beastMode.
     */
    void startBeastMode();
    /**
     * @brief startThreads Fonction qui démarre les threads.
     * Utile lors de redémarrage de partie.
     */
    void startThreads();

private:
    /**
     * @brief HEIGHT Constante représentant le nombre de ligne.
     */
    const int HEIGHT = 31; // nb cells lines
    /**
     * @brief WIDHT Constante représentant le nombre de colonne.
     */
    const int WIDHT = 28; // nb cells columns
    /**
     * @brief NB_GHOST Constante représentant le nombre de fantôme.
     */
    const int NB_GHOST = 4;
    /**
     * @brief MAX_LEVEL Constante représentant le nombre de niveau.
     */
    const int MAX_LEVEL = 255;
    /**
     * @brief MAX_LEVEL_BEASTMODE Constante représentant le nombre de level
     * pour l'efficacité du beastMode.
     */
    const int MAX_LEVEL_BEASTMODE = 19;
    /**
      * @brief map_ La map du jeu Pac-Man.
      */
    MapGame * map_ {nullptr};
    /**
      * @brief pacThread Thread gérant le déplacement du Pac-Man.
      */
    PacManThread * pacThread{nullptr};
    /**
      * @brief fruits Timer gérant l'apparation des fruits.
      */
    QTimer * fruits {nullptr} ;
    /**
      * @brief beatsMode Timer gérant le temps de beastMode.
      */
    QTimer * beastMode {nullptr};
    /**
      * @brief timerThread Timer gérant le temps
      */
    QTimer * timerThread {nullptr};
    /**
      * @brief intro Lecteur multimédia, permettant de lire le son d'acceuil.
      */
    QMediaPlayer *intro{nullptr};
    GhostsThread * gRThread{nullptr};
    GhostsThread * gOThread{nullptr};

    QFont pacFont_;
    /**
      * @brief level Niveau du jeu.
      */
    int level ;
    /**
      * @brief SIZEPIC Constante representant la taille d'un composant dans la vue.
      */
    const int SIZEPIC = 25;
    /**
     * @brief ghosts Conteneur de fantôme.
     */
    vector<shared_ptr<Ghost>> ghosts;
    /**
     * @brief pac Pac-Man.
     */
    shared_ptr<Pacman> pac;
    /**
     * @brief starter  Fonction qui permet la bonne intialisation
     * d'attribut et connexion. Lors de la construction du jeu.
     */
    void starter();

signals:
    /**
     * @brief stopped Signal notifiant l'arrêt des threads.
     */
    void stopped();
    /**
     * @brief emitFear Signal notifiant le mode beastMode et la peur des fantômes.
     */
    void emitFear();
};

#endif // GAME_H
