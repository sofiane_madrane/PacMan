#ifndef CHARACTER_H
#define CHARACTER_H
/*!
 *\file model/character.h
 */
#include "component.h"
#include "direction.h"

/*!
 * \class Character
 * \brief Cette classe abstraite (et mére) définis les entités, tel le joueur(Pac-Man) et les fantômes
 * dans le jeu Pac-Man. Elle gére  les caractéristiques de base des personnages.
 * \author Madrane, Taranto, Annu, Keenens, Ordonnez
 * \version 0.1
 * \date 2017
 */

class Character :public Component
{
protected:
    /**
      * @brief initPos Position initiale du personnage.
      */
    Position * initPos {nullptr};
    /**
      * @brief dir Direction de base du personnage.
      */
    Direction dir;
    /**
      * @brief nextDir Direction suivante du personnage.
      */
    Direction nextDir;
    /**
      * @brief alive Etat de vie du personnage.
      */
    bool alive;
    /**
      * @brief mapPos_ Position du personnage dans le tableau 2D.
      */
    Position * mapPos_ {nullptr};

public:
    /**
     * @brief Character Constructeur d'un personnage.
     * @param x Position en axe X  (destiné au tab 2D).
     * @param y Position en axe Y  (destiné au tab 2D).
     * @param pixX Position en axe X .
     * @param pixY Position en axe Y .
     * @param type Type du personnage .
     * @param dir La direction de base du personnage.
     */
    Character(int x, int y,float pixX, float pixY, MapComponent type, Direction dir);
    /**
     * @brief ~Character Destructeur du personnage.
     */
    virtual ~Character();
    /**
     * @brief getInitPosX Accesseur de la valeur
     * de la position initial de l'axe X.
     * @return La valeur X de la position initial.
     */
    float getInitPosX();
    /**
     * @brief getInitPosY Accesseur de la valeur de la
     * position initial de l'axe Y.
     * @return La valeur Y de la position initial.
     */
    float getInitPosY();
    /**
     * @brief getDirection Accesseur de la direction de base.
     * @return La direction de base.
     */
    Direction getDirection() const;
    /**
     * @brief setInitPosX Mutateur de la valeur de la
     * position initial de l'axe X.
     * @param pos La  nouvelle valeur X de la position initial.
     */
    void setInitPosX(float pos);
    /**
     * @brief setInitPosY Mutateur de la valeur de la
     * position initial de l'axe Y.
     * @param pos La  nouvelle valeur Y de la position initial.
     */
    void setInitPosY(float pos);
    /**
     * @brief setDirection Mutateur de la direction de base.
     * @param dir La nouvelle direction de base.
     */
    void setDirection(const Direction &dir);
    /**
     * @brief move Fonction qui permet de déplacer un personnage.
     * En fonction de sa direction de base.
     */
    void move();
    /**
     * @brief returnInitPos Fonction qui reinitialise la position courante
     * à la position initial du personnage.
     */
    void returnInitPos();
    /**
     * @brief getAlive Accesseur de l'état de vie du personnage.
     * @return L'état de vie du personnage.
     */
    bool getAlive() const;
    /**
     * @brief setAlive
     * @param value
     */
    void setAlive(bool value);
    /**
     * @brief setMapPosition Mutateur de la mapPosition.
     * @param pos La nouvelle mapPosition.
     */
    void setMapPosition(const Position &pos);
    /**
     * @brief getNextDir Accesseur de la direction suivante.
     * @return La direction suivante.
     */
    Direction getNextDir() const;
    /**
     * @brief setNextDir Mutateur de la direction suivante.
     * @param value La nouvelle direction suivante.
     */
    void setNextDir(const Direction &value);
    /**
     * @brief updateDir Fonction qui mets à jour la direction de base à la
     * direction suivante.
     */
    void updateDir();
    /**
     * @brief differentDir Fonction qui vérifie si la direction de base est
     * bien différente à la direction suivante.
     * @return La direction de base.
     */
    bool differentDir() const;
    /**
     * @brief getArrayPosition Accesseur de la position dans le tableau 2D.
     * @return La position dans le tableau 2D.
     */
    Position & getArrayPosition() const;
    /**
     * @brief eat Méthode virtuelle pure (abstraite).
     * De la faculté de manger d'un personnage.
     * @param c Un componsant.
     */
    virtual void eat(Component &c)=0 ;

};

#endif // CHARACTER_H
