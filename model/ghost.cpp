#include "ghost.h"



bool Ghost::getInit() const
{
    return init;
}

void Ghost::setInit(bool value)
{
    init = value;
}

bool Ghost::getFear() const
{
    return fear;
}

void Ghost::setFear(bool value)
{
    fear = value;
}

Ghost::Ghost(int x, int y, float pixX, float pixY, MapComponent type, Direction dir):
    Character(x,y,pixX,pixY,type,dir),scatterMode(true)
{
    init = true;
    fear = false;
    switch(type){
        case MapComponent::G_RED:
            color = 'r';
            scatterPos = {1*25,1*25};
            break;
        case MapComponent::G_PINK:
            color = 'p';
            scatterPos = {26*25,1*25};
            break;
        case MapComponent::G_BLUE:
            color = 'b';
            scatterPos = {1*25,29*25};
            break;
        case MapComponent::G_ORANGE:
            color = 'o';
            scatterPos = {26*25,29*25};
            break;
        default: scatterPos = {1,1};
    }
}

Ghost::~Ghost()
{

}

void Ghost::eat(Component &c)
{
    //Peut-etre synchronisez ça
    if(c.getType()!=MapComponent::PACMAN && scatterMode && !fear)
        return ;

    if(c.getPosition() == getPosition()) {
       ((Pacman&)c).setAlive(false);
       ((Pacman&)c).subLives();
    }
}

bool Ghost::getScatterMode() const
{
    return scatterMode;
}

void Ghost::setScatterMode(bool value)
{
    scatterMode = value;
}

Position Ghost::getScatterPos(){
    return this->scatterPos;
}
