#include "pacman.h"

Pacman::Pacman(int x, int y, float pixX, float pixY):
    Character(x,y,pixX,pixY,PACMAN,Direction::LEFT),
    lives(3),
    score(0),
    beastMode(false)

{
    this->eating = 0;
    this->maybeEat = true;
    this->maybeBeastMode = false ;
}

Pacman::~Pacman()
{
}

int Pacman::getLives()
{
    return lives;
}
void Pacman::setLive(int live)
{
    lives=live;
}
bool Pacman::isDead()
{
    return lives<=0;
}

int Pacman::getScore() const
{
    return score;
}

void Pacman::setScore(int value)
{
    score += value;
    cptLivesBonus+=value;
    if(cptLivesBonus ==10000){
        if(lives <6){
            lives+=1;
        }
        cptLivesBonus=0;
    }
}



bool Pacman::getBeastMode() const
{
    return beastMode;
}

void Pacman::setBeastMode(bool value)
{
    beastMode = value;
}

void Pacman::resetEating()
{
    eating = 0;
}

void Pacman::eat(Component &c)
{

    if((c.getType()== MapComponent::G_BLUE || c.getType()== MapComponent::G_ORANGE
            || c.getType()== MapComponent::G_PINK || c.getType()== MapComponent::G_RED))
        eatGhost((Ghost &)c);
    else
        if(c.getType()!= MapComponent::WALL
                && c.getType()!= MapComponent::EMPTY
                && c.getType()!= MapComponent::USE)
                        eatOthers(c);
}

bool Pacman::getMaybeEat() const
{
    return maybeEat;
}

void Pacman::setMaybeEat(bool value)
{
    maybeEat = value;
}

bool Pacman::getMaybeBeastMode() const
{
    return maybeBeastMode;
}

void Pacman::setMaybeBeastMode(bool value)
{
    maybeBeastMode = value;
}

void Pacman::resetScore()
{
    score = 0 ;
}

void Pacman::subLives()
{
    lives-=1;
}

void Pacman::eatGhost(Ghost &phantom)
{
    int valueScore = 0;
    if(phantom.getScatterMode() && beastMode && maybeEat) {
        //maybeEat devra être à faux a partir du niveau 19
        eating++;
        switch (eating) {
            case 1: valueScore = 200; break;
            case 2: valueScore = 400; break;
            case 3: valueScore = 800; break;
            case 4: valueScore = 1600; break;
            default:
                break;
        }
        setScore(valueScore);
        phantom.setScatterMode(false);
        phantom.setFear(true);
        emit scoreGhost(valueScore);
    }else {
        if (!phantom.fear){
            setAlive(false);
            subLives();
        }

    }
}

void Pacman::eatOthers(Component &c)
{
    setScore(c.getType());
    if (c.getType()==MapComponent::FLASH_BISCUIT && maybeEat) {
        maybeBeastMode = true ;
        setBeastMode(true);
    }
    if(c.getType() != MapComponent::FLASH_BISCUIT &&
            c.getType() != MapComponent::BISCUIT)
        c.setType(MapComponent::EMPTY); //CAS DES FRUITS
    else
        c.setType(MapComponent::USE);
}

void Pacman::moveMap(){
    switch (getDirection()){
        case Direction::UP:
        mapPos_->up();break;
        case Direction::DOWN:
        mapPos_->down();break;
        case Direction::LEFT:
        mapPos_->left();break;
        case Direction::RIGHT:
        mapPos_->right();break;
        default:break;
    }
}
