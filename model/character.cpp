#include "character.h"

Character::Character(int x, int y,float pixX, float pixY, MapComponent type, Direction dir):
    Component(pixX,pixY,type)
{
    this->dir = dir;
    this->nextDir = dir;
    this->initPos = new Position(pixX,pixY);
    this->mapPos_ = new Position(x,y);
    this->alive = true ;
    std::cout<<"Character 2"<<std::endl;
}

Character::~Character()
{
    if(initPos != nullptr)
        delete initPos;
    if (mapPos_ != nullptr)
        delete mapPos_;
}


float Character::getInitPosX()
{
    return initPos->getX();
}

float Character::getInitPosY()
{
    return initPos->getY();
}

Direction Character::getDirection() const
{
    return dir;
}

void Character::setInitPosX(float pos)
{
   initPos->setX(pos);

}

void Character::setInitPosY(float pos)
{
    initPos->setY(pos);
}

void Character::setDirection(const Direction& dir)
{
    this->dir=dir;
}

void Character::move()
{
    switch(dir)
    {
        case Direction::LEFT: pos->left();break;
        case Direction::RIGHT: pos->right();break;
        case Direction::UP: pos->up();break;
        case Direction::DOWN: pos->down();break;
    }
}

void Character::returnInitPos()
{
    setPosition((*this->initPos));
}

bool Character::getAlive() const
{
    return alive;
}

void Character::setAlive(bool value)
{
    alive = value;
}

void Character::setMapPosition(const Position &pos)
{
    *(this->mapPos_) = pos;
}

Direction Character::getNextDir() const
{
    return nextDir;
}

void Character::setNextDir(const Direction &value)
{
    nextDir = value;
}

void Character::updateDir()
{
    if(dir!=nextDir)
        dir = nextDir;
}

bool Character::differentDir() const
{
    return (nextDir != dir) ;
}

Position &Character::getArrayPosition() const
{
    return *mapPos_;
}
