#ifndef GHOSTPINK_H
#define GHOSTPINK_H
#include "ghost.h"

class GhostPink : public Ghost
{
private:
    const static MapComponent GPINK = MapComponent::G_PINK;
    Position walkPos = Position {1,1};

public:
    GhostPink(float x,float y);
    GhostPink(Position & pos);
};

#endif // GHOSTPINK_H
