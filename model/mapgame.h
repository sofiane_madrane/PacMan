#ifndef MAPGAME_H
#define MAPGAME_H
/*!
 *\file model/mapgame.h
 */
#include <memory>
#include <fstream>
#include <sstream>
#include <iostream>
#include <QDir>
#include <QtCore>
#include "pacman.h"
#include "ghost.h"

using namespace std;

/*!
 * \class mapgame
 * \brief cette classe represente la map dans le jeu Pac-Man.
 *
 * \author Madrane, Taranto, Annu, Keenens, Ordonnez
 * \version 0.1
 * \date 2017
 */
class MapGame
{
public:

    /**
     * @brief MapGame la carte de jeux
     * @param pac, le joueur
     * @param ghosts, les fantomes
     * @param sizePic , ta taille d'une cellule en pixels
     */
    MapGame(shared_ptr<Pacman> pac,  vector<shared_ptr<Ghost>> ghosts, int sizePic);

    /**
     * @brief create
     * @param path
     * @param sizePic
     */
    void create(std::string path, int sizePic);

    /**
     * @brief pacmanPos la position du pacman
     */
    Position pacmanPos;

    /**
     * @brief reloadMap, une méthode pour recharger la carte de jeu
     */
    void reloadMap();

    /**
     * @brief addSpecialItem, en fonction du niveau, un fruit est ajouté
     * @param level
     */
    void addSpecialItem(int level);

    /**
     * @brief SpecialItemGiver
     * @param level
     * @return the fruit to appears on the game to indicate the level
     */
    MapComponent SpecialItemGiver(int level);

    /**
     * @brief maybeAddFruits
     * @param level
     * @return true AND ADD the fruit at the map IF the number of dots already eat allows it
     */
    bool maybeAddFruits(int level);

    /**
     * @brief findDotsEnergizer
     * @return true if food ==0
     */
    bool findDotsEnergizer();

    /**
     * @brief getElementMap
     * @param pos
     * @return the component at the position pos
     */
    Component& getElementMap(const Position &pos);

    /**
     * @brief setComponent
     * to set the component cmpt at position pos
     * @param pos
     * @param cmpt
     */
    void setComponent(const Position &pos, const Component &cmpt );

    /**
     * @brief reInitAllGhosts at their origin position
     */
    void reInitAllGhosts();

    /**
     * @brief reInitPacman at his origin position
     */
    void reInitPacman();

    /**
     * @brief reInitAGhost
     * @param colorGhost, the color of the ghost
     */
    void reInitAGhost(const MapComponent & colorGhost);

    /**
     * @brief getMap
     * @return a vector of vector of Component. wich is the the contains of the map
     */
    const vector<std::vector<Component> > & getMap() const;

    /**
     * @brief closerPosDir,
     *
     * @param PossiblesWays
     * @param dirPossible
     * @param targetPos
     * @return the direction wich is the closest to the target pos compare to the others possibles ways
     */
    Direction closerPosDir(vector<Position> &PossiblesWays, vector<Direction> &dirPossible, Position targetPos);

    /**
     * @brief gap
     * @param a
     * @param b
     * @return the absolute gap between a and b
     */
    int gap(int a, int b);

    /**
     * @brief checkWays
     * check the 4 axes (top, left, right, bot) of the ghost
     * to know the possibilities it had to travel to the target
     * (the pos behing the ghost is reove so it cant go back)
     * @param ghost
     * @param dirPossible
     * @return all the possibles position
     */
    std::vector<Position> checkWays(Ghost &ghost, vector<Direction> & dirPossible);

    /**
     * @brief ghostMove
     * to move the ghost on the board. if it is on gost house, his position is forced
     * to get it out of it.
     * @param ghost
     * @param tPos
     */
    void ghostMove(Ghost &ghost, Position tPos );

    /**
     * @brief defineTarget
     * a method who switch on the != defineXtarget method.
     * following 'c' who represente the ghost
     * @param character, the player
     * @param c
     * @return the position of his target
     */
    Position defineTarget(Character &character,char c);

    /**
     * @brief defineRTarget
     * algo to find position where to go for RED ghost
     * @param character
     * @return the position of his target
     */
    Position defineRTarget(Character &character);//RED

    /**
     * @brief defineBTarget
     * algo to find position where to go for BLUE ghost
     * @param character
     * @param redGPos
     * @return the position of his target
     */
    Position defineBTarget(Character &character, Position redGPos);//BLEUE

    /**
     * @brief defineOPTarget
     * algo to find position where to go for ORANGE and PINK ghost
     * @param ghost
     * @param character
     * @param dist ( if 4, its pink. otherway its orange)
     * @return the position of his target
     */
    Position defineOPTarget(Ghost &ghost,Character &character, int dist);//PINK (dist = 4) && ORANGE (dist = 8)

    /**
     * @brief setScatterMode
     * to tell the ghosts they had to go on their scatter pos (each a corner)
     * @param b
     */
    void setScatterMode(bool b);

    /**
     * @brief getFood
     * @return the number of dors + energizer
     */
    int getFood() const;

    /**
     * @brief setFood
     * @param value
     */
    void setFood(int value);

    /**
     * @brief setFruitAppears
     * @param value
     */
    void setFruitAppears(bool value);

    /**
     * @brief getDots
     * @return the number of dots not already eat
     */
    int getDots() const;

    /**
     * @brief moveREDGhost
     * @param scatter
     * if scatter, go to his scatter pos
     */
    void moveREDGhost(bool scatter);

    /**
     * @brief moveOthersGhosts
     * @param scatter
     * if scatter, go to his scatter pos
     */
    void moveOthersGhosts(bool scatter);

    /**
     * @brief moveCharacter. to move any caracter of the game on the map.
     * ghost or pacman
     * @param character
     */
    void moveCharacter(Character &character);

    /**
     * @brief hungry eat the component and clean the pos
     * @param character
     */
    void hungry(Character &character);

    /**
     * @brief getGhosts
     * @return the ghosts
     */
    vector<shared_ptr<Ghost> > getGhosts() const;

private:
    /**
     * @brief ghosts_
     */
    vector<shared_ptr<Ghost>> ghosts_;

    /**
     * @brief player_
     */
    shared_ptr<Pacman> player_;

    /**
     * @brief map_ the board of the game
     */
    std::vector<std::vector<Component>> map_;

    /**
     * @brief dots, the things to eat
     */
    int dots ;

    /**
     * @brief food. the dors + energizer
     */
    int food ;

    /**
     * @brief ROW
     */
    const int ROW = 32;

    /**
     * @brief COLUMN
     */
    const int COLUMN = 28;

    /**
     * @brief SIZEPIC,
     */
    const int SIZEPIC = 25;

    /**
     * @brief NB_FOOD
     */
    const int NB_FOOD = 244 ;// 240 petites et 4 grosses

    /**
     * @brief NB_DOTS
     */
    const int NB_DOTS = 240;

    /**
     * @brief nbGhosts
     */
    const int nbGhosts = 4;

    /**
     * @brief findGhost
     * @param colorGhost
     * @return
     */
    int findGhost(const MapComponent & colorGhost);

    /**
     * @brief sceneToWorld
     * @param pos
     * @return the pos on the board by dividing the absolute pos on pixel by 25
     */
    Position sceneToWorld(const Position &pos);

    /**
     * @brief fruitAppears
     */
    bool fruitAppears ;

    /**
     * @brief cptAppaers
     */
    int cptAppaers ;

    /**
     * @brief isMovePossible
     * @param character
     * @param dir
     * @return true if the caracter can acces the way in the dir
     */
    bool isMovePossible(Character &character,Direction dir);
};
#endif // MAPGAME_H
