#include "ghoststhread.h"
#include <iostream>
#include <chrono>

using namespace std;
using namespace std::chrono;

GhostsThread::GhostsThread(Game *p, bool r)
{
    c = r;
    g = p ;
}

GhostsThread::~GhostsThread()
{
    cout<<"je suis détruit(ghostsTreads) baaah"<<endl;
}

void GhostsThread::run()
{
    while(g->getBegin() !=7);

    high_resolution_clock::time_point t1 = high_resolution_clock::now();
    high_resolution_clock::time_point t2;

    if(c == true){
        while(!g->pac->isDead()){
            QMutex mutex;
            mutex.lock();
            t2 = high_resolution_clock::now();
            if( duration_cast<seconds>(t2-t1).count() >= 7){
                g->getMapGame()->moveREDGhost(false);
                if( duration_cast<seconds>(t2-t1).count() >= 27)
                    t1 = high_resolution_clock::now();
            }else{
                g->getMapGame()->moveREDGhost(true);
            }
            mutex.unlock();
            usleep(40000);
        }
    }else{
        while(!g->pac->isDead()){
            QMutex mutex;
            mutex.lock();
            t2 = high_resolution_clock::now();
            if( duration_cast<seconds>(t2-t1).count() >= 7){
                g->getMapGame()->moveOthersGhosts(false);
                if( duration_cast<seconds>(t2-t1).count() >= 27)
                    t1 = high_resolution_clock::now();
            }else{
                g->getMapGame()->moveOthersGhosts(true);
            }

            mutex.unlock();
            usleep(45000);
        }
    }
}

