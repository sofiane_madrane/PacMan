#ifndef GHOSTSTREADS_H
#define GHOSTSTREADS_H

/*!
 *\file model/ghostsThread.h
 */

#include "game.h"
class Game;
/*!
 * \class GhostsThread
 * \brief thread de déplacement des ghosts
 * \author Madrane, Taranto, Annu, Keenens, Ordonnez
 * \version 0.1
 * \date 2017
 */
class GhostsThread : public QThread
{
public:

    /**
     * @brief GhostsThread
     * the gestion of the move of the gost
     * @param g, the game to follow , where the ghost will move
     * @param r, the ghost to move
     */
    GhostsThread(Game * g,bool r);

    ~GhostsThread();

    /**
     * @brief run metod
     * 5 seconds scatter and 20 sec
     * move every 40000 microsec for red and 45000 for others
     */
    void run();

private:

    /**
     * @brief r, true if red, false for others ghosts (to set the speed)
     */
    char c;

    /**
     * @brief g, the game to move the ghost
     */
    Game * g;
};

#endif // GHOSTSTREADS_H
