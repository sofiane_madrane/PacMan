#ifndef DIRECTION_H
#define DIRECTION_H
/*!
 *\file model/direction.h
 */


/**
 * @brief Enumération des différents types de direction possible.
 */
enum class Direction {
    LEFT ,
    RIGHT,
    UP ,
    DOWN
};

#endif // DIRECTION_H
