#include "mapgame.h"
#include <unistd.h>
#include <time.h>
#include <stdlib.h>
MapGame::MapGame(shared_ptr<Pacman> pac, vector<shared_ptr<Ghost> > ghosts, int sizePic) :
    player_(pac)
{
    cptAppaers = 0;
    food = NB_FOOD ;
    dots = NB_DOTS ;
    fruitAppears = false ;
     ghosts_.resize(ghosts.size());
    for (decltype(ghosts.size()) i = 0; i < ghosts.size(); i++) {
         ghosts_[i] = ghosts[i];
    }
    create(QDir::currentPath().toStdString() + "/ressources/niveau.txt",sizePic);
}

void MapGame::create(string path, int sizePic)
{
    ifstream file(path);
    cout<<boolalpha<<file.is_open()<<endl;
    if(file){
        cout<<"est ouvert"<<path<<endl;
    }else{
        cout<<"erreur : "<<path<<endl;
    }
    string str;
    map_.resize(ROW);
    try{
        unsigned cptLine = 0;
        while(getline(file,str))
        {
            map_[cptLine].resize(COLUMN);
            for(decltype(str.length()) i = 0; i < str.length(); i++) {
                switch(str[i]) {
                case 'J': //map_[cptLine].at(i)=  *player_;break;
                    //Component(cptLine,i,MapComponent::PACMAN) ;break;
                case ' ': map_[cptLine].at(i)= Component(i*sizePic, cptLine*sizePic, MapComponent::EMPTY); break;
                case 'w':map_[cptLine].at(i)=  Component(i*sizePic, cptLine*sizePic, MapComponent::WALL);break;
               /* case 'R':map_[cptLine].at(i)=  *ghosts_.at(0);break;
                    //Component(cptLine,i,MapComponent::G_RED);break;
                case 'B':map_[cptLine].at(i)=  *ghosts_[1];break;
                    //Component(cptLine,i,MapComponent::G_BLUE);break;
                case 'P':map_[cptLine].at(i)=  *ghosts_[2];break;
                    //Component(cptLine,i,MapComponent::G_PINK);break;
                case 'O':map_[cptLine].at(i)=  *ghosts_[3];break;
                    //Component(cptLine,i, MapComponent::G_ORANGE);break;*/
                case '.':map_[cptLine].at(i)=  Component(i*sizePic, cptLine*sizePic, MapComponent::BISCUIT);break;
                case 'o':map_[cptLine].at(i)=  Component(i*sizePic, cptLine*sizePic, MapComponent::FLASH_BISCUIT);break;
                default: cout<<"Lettre inconnue"<<endl;
                }
                //cout<<getText(map[cptLine][map[cptLine].size()-1]);
            }
            cptLine++;
        }
    }catch(std::exception e){
        cout<<"Il y a eu une erreur"<<e.what()<<endl;
    }
}

void MapGame::addSpecialItem(int level)
{
    QMutex mutex ;
    mutex.lock();
    map_[17][14].setType(SpecialItemGiver(level));
    mutex.unlock();
}

MapComponent MapGame::SpecialItemGiver(int level){
    switch(level) {
    //CAS OU LE FRUIT VA DISPARAITRE
    case 0 : return MapComponent::EMPTY;
    case 1 : return MapComponent::CHERRY;
    case 2 : return MapComponent::STRAWBERRY;
    case 3 :
    case 4 : return MapComponent::ORANGE;
    case 5 :
    case 6 : return MapComponent::APPLE;
    case 7 :
    case 8 : return MapComponent::PINEAPPLE;
    case 9 :
    case 10 : return MapComponent::GALAXIAN;
    case 11 :
    case 12 : return MapComponent::BELL;
    default : return MapComponent::KEY;
    }
}

bool MapGame::maybeAddFruits(int level)
{
    /*si (dotsMangés-flashMangés == 70) alors fruit*/
    bool ret = (((dots == (NB_DOTS - 70) && cptAppaers == 0)
                 || (dots == (NB_DOTS - 170) && cptAppaers ==1))
                    && !fruitAppears) ;
    if ( ret ) {
        fruitAppears = true ;
        addSpecialItem(level);
        cptAppaers+=1;
    }
    return ret ;
}

//Dans game apres l'appel à cette methode verifier la fin du jeu
// pour reinitialiser les positions aves les méthodes reInitXXXXXX
void MapGame::hungry(Character &character){
    Component oldC;
    Position newPos = character.getPosition();

    oldC = getElementMap(character.getPosition());
    newPos = character.getPosition();
    Component passed (newPos,MapComponent::USE);
    if(character.getType() == MapComponent::PACMAN){
        if(oldC.getType() == MapComponent::BISCUIT
                || oldC.getType() == MapComponent::FLASH_BISCUIT){
            food-=1;
            if (oldC.getType() == MapComponent::BISCUIT){
                std::cout<<"Mange"<<std::endl;
                dots-=1 ;
            }
        }
        character.eat(oldC);
        passed = oldC ;
        setComponent(newPos,passed);
        unsigned i =0 ;
        //Pour pouvoir manger un ou des phantomes
        while ((character.getAlive() /*|| !found*/) && i < ghosts_.size() ) {
            if(character.getArrayPosition() == ghosts_[i]->getArrayPosition())
                character.eat(*ghosts_[i]);
            i++;
        }
    }else{
        //c'est un fantôme
        //character.eat(*player_);
    }
}
bool MapGame::isMovePossible(Character &character,Direction dir){
    bool value = true;
    switch(dir){
        case Direction::LEFT:
            if(map_[character.getArrayPosition().getY()]
                    [character.getArrayPosition().getX()-1].getType() ==-1){
            value = false;
            }break;
        case Direction::RIGHT:
            if(map_[character.getArrayPosition().getY()]
                    [character.getArrayPosition().getX()+1].getType() ==-1){
            value = false;
            }break;
        case Direction::UP:
            if(map_[character.getArrayPosition().getY()-1]
                    [character.getArrayPosition().getX()].getType() ==-1){
            value = false;
            }break;
        case Direction::DOWN:
            if(map_[character.getArrayPosition().getY()+1]
                    [character.getArrayPosition().getX()].getType() ==-1){
            value = false;
            }break;
        default : value = true;
    }
    return value;
}
void MapGame::moveCharacter(Character &character){
    //D'abord vérifier si le personnage est en transition de cellule ou non

    int pixX =int(character.getPosition().getX());
    int pixY =int(character.getPosition().getY());

    if(character.getPosition().getX() < 10 && character.getPosition().getY()==350 && character.getDirection() == Direction::LEFT){
        //std::cout<<"TP"<<std::endl;
        character.setPosition(Position(690,350));
    }else if(character.getPosition().getX() > 690 && character.getPosition().getY()==350 && character.getDirection() == Direction::RIGHT){
        //std::cout<<"TP"<<std::endl;
        character.setPosition(Position(10,350));
    }



    if(pixX %SIZEPIC==0 && pixY %SIZEPIC==0){
        //Mettre à jour la position TABLEAU du caractère.
        character.getArrayPosition().setX(pixX/25);
        character.getArrayPosition().setY(pixY/25);
            // Verifie l'etat de la cellule où aller en fonction de la direction.
            if(character.getDirection()==character.getNextDir()){
                if(isMovePossible(character,character.getDirection())){
                    character.move();
                }
            }else{
                if(isMovePossible(character,character.getNextDir())){
                    character.updateDir();
                    character.move();
                }
                if(isMovePossible(character,character.getDirection())){
                    character.move();
                }
            }
    }else{
        character.move();
    }
}
vector<shared_ptr<Ghost> > MapGame::getGhosts() const
{
    return ghosts_;
}
//==========================================================================================//
std::vector<Position> MapGame::checkWays(Ghost &ghost, vector<Direction> &dirPossible){
    std::vector<Position> PossiblesWays;
    //int pixX =int(ghost.getPosition().getX());
    //int pixY =int(ghost.getPosition().getY());

    if(ghost.getPosition().getX() < 1 && ghost.getPosition().getY()==350 && ghost.getDirection() == Direction::LEFT){
        //std::cout<<"TP"<<std::endl;
        ghost.setPosition(Position(700,350));
        ghost.setDirection(Direction::LEFT);

    }else if(ghost.getPosition().getX() >= 695 && ghost.getPosition().getY()==350 && ghost.getDirection() == Direction::RIGHT){
        //std::cout<<"TP"<<std::endl;
        ghost.setPosition(Position(0,350));
        ghost.setDirection(Direction::RIGHT);
    } else {

        int pixX =int(ghost.getPosition().getX());
        int pixY =int(ghost.getPosition().getY());

        ghost.getArrayPosition().setX(pixX/25);
        ghost.getArrayPosition().setY(pixY/25);

        if(isMovePossible(ghost,Direction::LEFT) && (ghost.getDirection() !=Direction::RIGHT)
                && ghost.getArrayPosition().getX() > 0){
            PossiblesWays.push_back(map_[ghost.getArrayPosition().getY()]
                    [ghost.getArrayPosition().getX()-1].getPosition());
            dirPossible.push_back(Direction::LEFT);
        }
        if(isMovePossible(ghost,Direction::RIGHT)&& ghost.getDirection() !=Direction::LEFT
                && ghost.getArrayPosition().getX() < 27 ){
            PossiblesWays.push_back(map_[ghost.getArrayPosition().getY()]
                    [ghost.getArrayPosition().getX()+1].getPosition());
            dirPossible.push_back(Direction::RIGHT);
        }
        if(isMovePossible(ghost,Direction::UP)&& ghost.getDirection() !=Direction::DOWN){
            PossiblesWays.push_back(map_[ghost.getArrayPosition().getY()-1]
                    [ghost.getArrayPosition().getX()].getPosition());
            dirPossible.push_back(Direction::UP);
        }
        if(isMovePossible(ghost,Direction::DOWN)&& ghost.getDirection() !=Direction::UP){
            PossiblesWays.push_back(map_[ghost.getArrayPosition().getY()+1]
                    [ghost.getArrayPosition().getX()].getPosition());
            dirPossible.push_back(Direction::DOWN);
        }
    }
    return PossiblesWays;
}

//red
Position MapGame::defineRTarget(Character &character){
    return character.getPosition();
}
//blue
Position MapGame::defineBTarget(Character &character, Position redGPos){
    Position p = character.getPosition();
    redGPos = ghosts_[0]->getPosition();
    int xGap = gap(p.getX(), redGPos.getX());
    int yGap = gap(p.getY(), redGPos.getY());
    float retX, retY;
    if((p.getX() - redGPos.getX())>0){
        //ghost + bas que le point devant le pacman
        retX = redGPos.getX()+2*xGap;
    }else if((redGPos.getX() - p.getX())>0){
        //ghost + haut que le point devant le pacman
        retX = redGPos.getX()-2*xGap;
    }else{
        retX = redGPos.getX();
    }
    if((p.getY() - redGPos.getY())>0){
        //ghost à gauche du point devant le pacman du pacman
        retY = redGPos.getY()+2*yGap;
    }else if((redGPos.getY() - p.getY())>0){
        //ghost à droite du point devant le pacman du pacman
        retY = redGPos.getY()-2*yGap;
    }else{
        retY = redGPos.getY();
    }
    return {retX, retY};
}
//pink & orange
Position MapGame::defineOPTarget(Ghost &ghost, Character &character, int dist){
    Position p = character.getPosition();
    Position g = ghost.getPosition();
    Position retPos;
    switch(dist){
        //pink
        case 4://4tuiles de distance devant pacman
            switch(character.getDirection()){
                case Direction::UP:
                    retPos = {p.getX(),p.getY()-100};
                    break;
                case Direction::DOWN:
                    retPos = {p.getX(),p.getY()+100};
                    break;
                case Direction::LEFT:
                    retPos = {p.getX()-100,p.getY()};
                    break;
                case Direction::RIGHT:
                    retPos = {p.getX()+100,p.getY()};
                    break;
                default:;
            }
            break;
        //orange
        case 8://je t'aime ,moi non plus
            //si je suis trop pres( 8 cases), cassos
            if(gap(g.getX(),p.getX())>=200 || gap(g.getY(),p.getY())>=200)
                retPos = ghost.getScatterPos();//{1.0,1.0};
            else//sinon je le course!
                retPos = p;
            break;
        default:;
    }
    return retPos;
}

int MapGame::getFood() const
{
    return food;
}

void MapGame::setFood(int value)
{
    food = value;
}

Position MapGame::defineTarget(Character &character,char c){
    Position retPos;
    switch(c){
        case 'r': retPos = defineRTarget(character);
            break;
        case 'b': retPos = defineBTarget(character,{1,1});
            break;
        case 'p': retPos = defineOPTarget(*ghosts_[2],character, 4);
            break;
        case 'o': retPos = defineOPTarget(*ghosts_[3],character ,8);
            break;
        default:;
    }
    return retPos;
}

int MapGame::gap(int a, int b){
    if(a<0) a= a*(-1);
    if(b<0) b= b*(-1);
    if((a-b)<0) return b-a;
    else return a-b;
}

Direction MapGame::closerPosDir(vector<Position> &PossiblesWays, vector<Direction> &dirPossible, Position targetPos){
    std::vector<double> dists;
    double dis;
    Direction res;
    for(int i = 0; i< PossiblesWays.size();i++){
        //possibleWays.getX()+12 je centralise le point vers le centre de la casse à tester.
        dis = sqrt(pow((PossiblesWays[i].getX()+12) - targetPos.getX(), 2) + pow((PossiblesWays[i].getY()+12)- targetPos.getY(), 2));
        dists.push_back(dis);
    }
    //identifier la distance la plus court
    const std::vector<double>::iterator iter_min = std::min_element(dists.begin(),dists.end() ) ;
    const double min_element = *iter_min ;//element MIN
    const int min_element_pos = iter_min - dists.begin() ;
    res = dirPossible[min_element_pos];
    return res;
}

void MapGame::setScatterMode(bool b){
    for(unsigned i=0; i<ghosts_.size();i++){
        ghosts_[i]->setScatterMode(b);
    }
}

void MapGame::ghostMove(Ghost &ghost, Position tPos){

    //forcer la sortie de la maison
    if((ghost.getPosition().getX() ==12*25) && (ghost.getPosition().getY() ==14*25) ||
        (ghost.getPosition().getX() ==14*25) && (ghost.getPosition().getY() ==14*25) ||
        (ghost.getPosition().getX() ==16*25) && (ghost.getPosition().getY() ==14*25))
            ghost.setPosition({14*25,11*25});

    vector<Direction> dirPossible;
    std::vector<Position> PossiblesWays = checkWays(ghost,dirPossible);

    if (!PossiblesWays.empty()){
        if(PossiblesWays.size() > 1){
            dirPossible.clear();
            Direction dir = closerPosDir(PossiblesWays,dirPossible,tPos);
            ghost.setNextDir(dir);
        }else{
            ghost.setNextDir(dirPossible[0]);
        }
    }

    moveCharacter(ghost);
    PossiblesWays.clear();
    dirPossible.clear();
}

void MapGame::moveREDGhost(bool scatter)
{
    if(!scatter){
        ghostMove(*ghosts_[0], defineTarget(*player_,'r'));
    }else
        ghostMove(*ghosts_[0], ghosts_[0]->getScatterPos());
}

void MapGame::moveOthersGhosts(bool scatter)
{
    if(!scatter){
        ghostMove(*ghosts_[1], defineTarget(*player_,'p'));
        if(NB_DOTS - getDots()>=30){
            ghostMove(*ghosts_[2].get(), defineTarget(*player_,'b'));
        }
        if(getDots()<(2*(NB_DOTS/3))){
            ghostMove(*ghosts_[3].get(), defineTarget(*player_,'o'));
        }
    }else{
        ghostMove(*ghosts_[1], ghosts_[1]->getScatterPos());
        if(NB_DOTS - getDots()>=30){
            ghostMove(*ghosts_[2].get(), ghosts_[2]->getScatterPos());
        }
        if(getDots()<(2*(NB_DOTS/3))){
            ghostMove(*ghosts_[3].get(), ghosts_[2]->getScatterPos());
        }
    }

}
//==========================================================================================//
bool MapGame::findDotsEnergizer()
{
    return food == 0;
}

void MapGame::reloadMap()
{
    dots = NB_DOTS ;
    food = NB_FOOD ;
    cptAppaers = 0;
    fruitAppears = false ;
    //Reinit Pacman
    reInitPacman();
    //REINIT les ghosts_ en mettant leurs positions initial dans la map à vide
    reInitAllGhosts();


    for(int i = 1; i<ROW-1;i++){
        for(int j = 1;j<COLUMN-1; j++) {
            if(map_[i][j].getType() == MapComponent::USE){
                if( (j==1 || j==26) && ( i==3 || i==23))
                    map_[i][j].setType(MapComponent::FLASH_BISCUIT);
                else
                    map_[i][j].setType(MapComponent::BISCUIT);
            }
        }
    }
}

Component &MapGame::getElementMap(const Position &pos)
{
    auto index = sceneToWorld(pos);
    return map_[index.getY()][index.getX()];
}

void MapGame::setComponent(const Position &pos, const Component &cmpt)
{
    auto index = sceneToWorld(pos); //AUTO à enlever
    map_[index.getY()][index.getX()] = cmpt;
}

void MapGame::reInitPacman()
{
    player_->returnInitPos();
}

void MapGame::reInitAGhost(const MapComponent &colorGhost)
{
    int ind = findGhost(colorGhost); //Parcours en plus pour  trouver le fantome
    ghosts_[ind]->returnInitPos();
    ghosts_[ind]->setScatterMode(false);
}

void MapGame::reInitAllGhosts(){
    for(decltype(ghosts_.size()) i = 0; i < ghosts_.size(); i++) {
        ghosts_[i]->returnInitPos();
        ghosts_[i]->setScatterMode(false);
    }
 }

const vector<std::vector<Component> > &MapGame::getMap() const
{
    return map_;
}

int MapGame::findGhost(const MapComponent &colorGhost)
{
    decltype(ghosts_.size()) i = 0;
    bool ret = false;
    while (i < ghosts_.size() && !ret){
        //if (ghosts_[i].getType() == colorGhost)
        if (ghosts_[i]->getType() == colorGhost)
            ret = true;
        else
            i++;
    }
    return i;
}

//position en pixel dans le tableau
Position MapGame::sceneToWorld(const Position &pos)
{
    int x ,y;
    x = pos.getX()/25.0;
    y = pos.getY()/25.0;
    Position world(x, y);
    return world;
}

void MapGame::setFruitAppears(bool value)
{
    fruitAppears = value;
}

int MapGame::getDots() const
{
    return dots;
}
