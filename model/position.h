#ifndef POSITION_H
#define POSITION_H
/*!
 *\file model/position.h
 */
#include <iostream>
#include <ostream>

/*!
 * \class Position
 * \brief cette classe permet  de modéliser une position dans le jeu Pac-Man.
 * En pixel ou en "arrayPosition"
 * \author Madrane, Taranto, Annu, Keenens, Ordonnez
 * \version 0.1
 * \date 2017
 */

class Position
{
public:
    /**
     * @brief Position Constructeur par défault d'une position.
     */
    Position();
    /**
     * @brief Position Constructeur d'une position d'une position.
     * @param x Position en axe X.
     * @param y Position en axe Y.
     */
    Position(float x, float y);
    /**
     * @brief Position Constructeur de recopie d'une position.
     * @param pos Une position.
     */
    Position(const Position &pos);
    /**
     * @brief getX Accesseur de la position de l'axe X.
     * @return Position de l'axe X.
     */
    float getX() const;
    /**
     * @brief setX Mutateur de la position de l'axe X.
     * @param value La nouvelle valeur de l'axe X.
     */
    void setX(float value);
    /**
     * @brief getY Accesseur de la position de l'axe Y.
     * @return Position de l'axe Y.
     */
    float getY() const;
    /**
     * @brief setX Mutateur de la position de l'axe Y.
     * @param value La nouvelle valeur de l'axe Y.
     */
    void setY(float value);
    /**
     * @brief left Fonction qui effectue un déplacement vers la gauche.
     */
    void left();
    /**
     * @brief right Fonction qui effectue un déplacement vers la droite.
     */
    void right();
    /**
     * @brief up Fonction qui effectue un déplacement vers le haut.
     */
    void up();
    /**
     * @brief down Fonction qui effectue un déplacement vers le bas.
     */

    void down();
    /**
     * @brief operator == Surchage de l'opérateur. Vérifie la bonne égalité de deux positions.
     * @param lhs Une position.
     * @param rhs Une position.
     * @return Bien égaux
     */
    friend bool operator==(const Position &lhs, const Position &rhs);
    /**
     * @brief operator != Surcharge de l'opérateur. Vérifie la différence de deux positions.
     * @param lhs Une position.
     * @param rhs Une position
     * @return Bien différents.
     */
    friend bool operator!=(const Position &lhs, const Position &rhs);

private:
    /**
     * @brief x Valeur de l'axe X.
     */
    float x ;
    /**
     * @brief y Valeur de l'axe Y.
     */
    float y ;
};

#endif // POSITION_H
