#include "game.h"
#include "ghoststhread.h"

using namespace std;

Game::Game()
{
    level = 1 ;
    pac = make_shared <Pacman>(14,23,14*SIZEPIC, 23*SIZEPIC);
    ghosts.resize(NB_GHOST);

    Position pos(1,1);// Mettre ca dans leurs propres constructeurs et non l'envoyer en args.

    ghosts.at(0)= make_shared<Ghost>(14,11,14*SIZEPIC, 11*SIZEPIC,MapComponent::G_RED,Direction::LEFT);
    ghosts.at(1)= make_shared<Ghost>(12,14,12*SIZEPIC, 14*SIZEPIC,MapComponent::G_ORANGE,Direction::UP);
    ghosts.at(2)= make_shared<Ghost>(14,14,14*SIZEPIC, 14*SIZEPIC,MapComponent::G_BLUE,Direction::UP);
    ghosts.at(3)= make_shared<Ghost>(16,14,16*SIZEPIC, 14*SIZEPIC,MapComponent::G_PINK,Direction::DOWN);

    map_ = new MapGame(pac,ghosts,SIZEPIC);


    //Initialisation de la police de caractères
    int id = QFontDatabase::addApplicationFont(":/PacFont");
    pacFont_ =(QFontDatabase::applicationFontFamilies(id).at(0));
    pacFont_.setPixelSize(25);

    starter();
}
void Game::starter(){

    pacThread = new PacManThread(this);
    fruits = new QTimer(this);
    beastMode = new QTimer(this);
    timerThread = new QTimer(this);


    gOThread = new GhostsThread(this, true);
    gRThread = new GhostsThread(this, false);

    connect(fruits,SIGNAL(timeout()),this,SLOT(resetFruit()));
    connect(beastMode,SIGNAL(timeout()),this,SLOT(resetBeastMode()));
    connect(timerThread,SIGNAL(timeout()),this,SLOT(startThreads()));
    connect(pacThread,SIGNAL(emitNotify()),this,SLOT(notify()));
    connect(pacThread,SIGNAL(emitFruitTimer()),this,SLOT(startFruitsTimer()));
    connect(pacThread,SIGNAL(emitBeastModeTimer()),this,SLOT(startBeastMode()));
    connect(this,SIGNAL(stopped()),pacThread,SLOT(terminate()));
    connect(this,SIGNAL(stopped()),gRThread,SLOT(terminate()));
    connect(this,SIGNAL(stopped()),gOThread,SLOT(terminate()));

    intro = new QMediaPlayer();
    intro->setVolume(70);
    intro->setMedia(QUrl("qrc:/begin"));
    intro->play();
    pacThread->start(QThread::LowestPriority);

    gRThread->start(QThread::LowestPriority);
    gOThread->start(QThread::LowestPriority);
}


Game::~Game()
{
    if (map_ != nullptr)
        delete map_;

    if (fruits != nullptr)
        delete fruits ;

    if (beastMode != nullptr)
        delete beastMode ;

    if (timerThread != nullptr)
        delete timerThread ;

    if (pacThread != nullptr)
        delete pacThread;

    if (intro != nullptr)
        delete intro;
}

void Game::movePacman(Direction  dir)
{
    pac->setNextDir(dir);
    //notifierChangement(); //?S
}

void Game::nextLevel()
{
     emit stopped();
     timerThread->start(3000);
     map_->addSpecialItem(0);
     fruits->stop(); // On arrete le timerS
     map_->setFruitAppears(false);
     resetBeastMode();
     level++;
     map_->reloadMap();
     pac->setDirection(Direction::LEFT);
     pac->setNextDir(Direction::LEFT);
}

bool Game::maybeNextLevel()
{
    return pac->getAlive() && map_->findDotsEnergizer() ;
}

void Game::restartGame()
{
    emit stopped();
    timerThread->start(3000);
    map_->addSpecialItem(0);
    fruits->stop(); // On arrete le timerS
    map_->setFruitAppears(false);
    resetBeastMode();
    level=1;
    pac->resetScore();
    pac->setLive(3);
    map_->reloadMap();
    pac->setDirection(Direction::LEFT);
    pac->setNextDir(Direction::LEFT);
}

MapGame *Game::getMapGame()
{
    return map_;
}

QFont Game::getpacFont()
{
    return pacFont_;
}

int Game::getLevel()
{
    return level;
}

int Game::getBegin()
{
    return intro->mediaStatus();
}

shared_ptr<Pacman> Game::getPlayer() const
{
    return pac;
}

void Game::resetFruit()
{
    /*Dans addSpecialItem la valeur 0
     * permettra de reinitialiser la case à empty*/
    map_->addSpecialItem(0);
    fruits->stop(); // On arrete le timerS
    map_->setFruitAppears(false);

    notifierChangement();
}

void Game::resetBeastMode()
{
    /*TODO*/
    pac->setBeastMode(false);
    pac->resetEating();
    for (decltype(ghosts.size()) i = 0; i < ghosts.size(); i++) {
        ghosts[i]->setScatterMode(false);
        ghosts[i]->setFear(false);
    }
    beastMode->stop();
}

void Game::notify()
{  
    notifierChangement();
}

void Game::startFruitsTimer()
{
        fruits->start(10000);
}

void Game::startBeastMode()
{
    int delay ;

    pac->setMaybeBeastMode(false);
    pac->setBeastMode(true);
    if(level > MAX_LEVEL_BEASTMODE) {
        pac->setMaybeEat(false);
        delay = 3500;
    } else
        delay = 10000; //Avant le nv 19

    for (unsigned i = 0; i < ghosts.size(); i++) {
        ghosts[i]->setScatterMode(true);
    }
    beastMode->start(delay);
    emit emitFear();
}


void Game::startThreads()
{
    pacThread->start(QThread::LowestPriority);
    gOThread->start(QThread::LowestPriority);
    gRThread->start(QThread::LowestPriority);
    timerThread->stop();
}
vector<shared_ptr<Ghost> > Game::getGhosts() const
{
    return ghosts;
}
