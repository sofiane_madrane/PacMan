#ifndef MAPCOMPONENT_H
#define MAPCOMPONENT_H
/*!
 *\file model/mapcomponent.h
 */

/**
 * @brief Enumération des types composants de la map.
 */
enum  MapComponent
{
    WALL=-1,
    G_RED,
    G_BLUE,
    G_ORANGE,
    G_PINK,
    PACMAN,
    BISCUIT=10,
    FLASH_BISCUIT=50,
    CHERRY = 100,
    STRAWBERRY = 300,
    ORANGE = 500,
    APPLE = 700,
    PINEAPPLE = 1000,
    GALAXIAN = 2000,
    BELL = 3000,
    KEY = 5000,
    EMPTY,
    USE
};

#endif // MAPCOMPONENT_H
