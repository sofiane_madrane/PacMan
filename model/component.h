#ifndef COMPONENT_H
#define COMPONENT_H
/*!
 *\file model/component.h
 */

#include "position.h"
#include "mapcomponent.h"

/*!
 * \class Component
 * \brief cette classe represente tout les éléments trouvant sur la map dans le jeu Pac-Man.
 * Classe mére de tout objet se trouvant sur la map.
 * \author Madrane, Taranto, Annu, Keenens, Ordonnez
 * \version 0.1
 * \date 2017
 */

class Component {
protected:
   /**
     * @brief pos Représente la position de chaque élément se trouvant sur la map.
     */
   Position * pos{nullptr};
private :
   /**
    * @brief type Type de l'élément représenté.
    */
   MapComponent type;

public:
   /**
    * @brief Component Constructeur par défault d'un componsant.
    */
   Component();
   /**
    * @brief Component Constructeur d'un componsant.
    * @param x  Sa position sur l'axe X.
    * @param y  Sa position sur l'axe Y.
    * @param type Le type representé.
    */
   Component(float x, float y, MapComponent type);
   /**
    * @brief Component Constructeur d'un componsant.
    * @param pos  Position x y.
    * @param type Le type representé.
    */
   Component (const Position &pos, MapComponent type) ;
   /**
    * @brief Component Constructeur de recopie d'un componsant.
    * @param cmpt Componsant a recopié.
    */
   Component (const Component & cmpt);
   /**
     * @brief ~Component Destructeur d'un componsant.
     */
   ~Component();
   /**
    * @brief getPosition Accesseur de la position d'un componsant.
    * @return Sa position.
    */
   Position & getPosition() const;
   /**
    * @brief setPosition Mutateur de la position d'un componsant.
    * @param pos La nouvelle position.
    */
   void setPosition(const Position & pos);
   /**
    * @brief getType Accesseur du type d'un componsant.
    * @return Son type.
    */
   MapComponent getType()const;
   /**
    * @brief setType Mutateur du type d'un componsant.
    * @param value Le nouvau type.
    */
   void setType(const MapComponent &value);
   /**
    * @brief operator = Surcharge de l'opérateur d'affectation.
    * @param cmpt Le composant a copié.
    * @return Le composant affecté.
    */
   Component& operator=(const Component & cmpt);
};

#endif // COMPONENT_H
