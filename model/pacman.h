#ifndef PACMAN_H
#define PACMAN_H
/*!
 *\file model/pacman.h
 */
#include <QtCore>
#include <vector>
#include <memory>
#include <QMediaPlayer>
#include "character.h"
#include "ghost.h"

/*!
 * \class Pacman
 * \brief Cette classe représente le joueur(Pac-Man) dans le jeu Pac-Man.
 *  Elle gére toutes les caractéristiques du personnage.
 * \author Madrane, Taranto, Annu, Keenens, Ordonnez
 * \version 0.1
 * \date 2017
 */

class Ghost;

class Pacman : public QObject ,public Character
{
    Q_OBJECT
private:
    /**
     * @brief PACMAN Constante définissant le type du Pacman.
     */
    const static MapComponent PACMAN = MapComponent::PACMAN ;
    /**
     * @brief lives Nombre de vie restant à Pac-Man.
     */
    int lives ;
    /**
     * @brief score Score de Pac-Man.
     */
    int score ;
    /**
     * @brief beastMode Mode capable de manger les fantômes.
     */
    bool beastMode;
    /**
     * @brief maybeBeastMode Etat de la possibilité de passer en beastMode.
     */
    bool maybeBeastMode ;
    /**
     * @brief maybeEat Etat de la possibilité de manger des fantômes.
     * A partir du niveau 19, le beastMode n'a plus aucun effet.
     */
    bool maybeEat; //maybeEat devra être à faux a partir du niveau 19
    /**
     * @brief eating Nombre de fantôme manger durant un "beastMode"
     * Valeur reinitialisée à chaque "beastMode".
     */
    int eating;
    /**
     * @brief eatGhost Fonction qui permet de pouvoir manger un fantôme.
     * @param phantom Fantôme a mangé.
     */
    void eatGhost(Ghost &phantom);
    /**
     * @brief eatOthers Fonction nourrit Pac-Man avec les items disposés sur la map.
     * @param c Item susceptible d'être consommer
     */
    void eatOthers(Component &c);


public:
    /**
     * @brief Pacman Construis le Pac-Man.
     * @param x Sa position en axe X -> j (arrayPos).
     * @param y Sa position en axe Y -> i(arrayPos).
     * @param pixX Sa position en axe X (pixel).
     * @param pixY Sa Position en axe Y (pixel).
     */
    Pacman(int x, int y, float pixX, float pixY);
    /**
     * @brief ~Pacman Destructeur de Pacman.
     */
    virtual ~Pacman();
    /**
     * @brief getLives Accesseur de la vie du Pac-Man.
     * @return  Le nombre de vie restante.
     */
    int getLives();
    /**
     * @brief cptLivesBonus  Score pour la vie bonus.
     */
    int cptLivesBonus=0;
    /**
     * @brief setLive Mutateur de la vie de Pac-Man.
     * @param live Valeur de la vie du Pac-Man.
     */
    void setLive(int live);
    /**
     * @brief isDead Accesseur sur l'état de vie de Pac-Man
     * Equivalent à game-over.
     * @return Pac-Man est mort.
     */
    bool isDead();
    /**
     * @brief getScore Accesseur du score.
     * @return  Le score.
     */
    int getScore() const;
    /**
     * @brief setScore Mutateur du score.
     * @param value Le score a ajouté
     * Il ajoute value au score de base.
     */
    void setScore(int value);
    /**
     * @brief getBeastMode
     * @return
     */
    bool getBeastMode() const;
    /**
     * @brief setBeastMode
     * @param value
     */
    void setBeastMode(bool value);
    /**
     * @brief Fonction qui remet à zero le nombre de fantôme manger lors
     * du beastMode
     */
    void resetEating();
    /**
     * @brief moveMap Fonction qui déplace Pac-Man en fonction de sa direction
     */
    void moveMap();
    /**
     * @brief eat Fonction qui permet de manger n'importe quelle item de map.
     * Qui est consommable.
     * @param c Item susceptible d'être manger.
     */
    virtual void eat(Component &c) override;
    /**
     * @brief getMaybeEat Accesseur de l'état  possibilité de manger des fantômes.
     * @return Etat de la  possibilité de manger des fantômes.
     */
    bool getMaybeEat() const;
    /**
     * @brief setMaybeEat Mutateur de l'état de la  possibilité de manger des fantômes.
     * @param value  Possibilité de manger des fantômes.
     */
    void setMaybeEat(bool value);
    /**
     * @brief getMaybeBeastMode Accesseur de l'état de la de passer en beastMode.
     * @return Etat de la possibilité de passer en beastMode.
     */
    bool getMaybeBeastMode() const;
    /**
     * @brief setMaybeBeastMode Mutateur de l'état de la de passer en beastMode.
     * @param value  Possibilité de passer en beastMode.
     */
    void setMaybeBeastMode(bool value);
    /**
     * @brief resetScore Remets le score à zéro.
     */
    void resetScore();
    /**
     * @brief subLives Décremente la vie de Pac-Man de un.
     */
    void subLives();

signals:
    /**
     * @brief scoreGhost Signal notifiant la vue du score apporter par la consommation
     * d'un fantôme.
     * @param value Score du fantôme.
     */
    void scoreGhost(int value);
};

#endif // PACMAN_H
