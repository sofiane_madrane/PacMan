#ifndef GHOST_H
#define GHOST_H

/*!
 *\file model/ghost.h
 */

#include "character.h"
#include "pacman.h"
#include "position.h"
#include <string>

class Pacman;

/*!
 * \class Ghost
 * \brief ghost who wants to eat pacman
 * \author Madrane, Taranto, Annu, Keenens, Ordonnez
 * \version 0.1
 * \date 2017
 */

class Ghost : public Character
{    

public:
    /**
     * @brief Ghost
     * @param x Position on absyss axe (to 2D array).
     * @param y Position on ordonee axe  (to 2D array).
     * @param pixX Position x on axe.
     * @param pixY Position on y axe.
     * @param type, the color of the ghost. red, blue, pink, or orange
     * @param dir, its begining direction
     */
    Ghost(int x, int y,float pixX, float pixY, MapComponent type, Direction dir);

    /**
     * @brief ~Ghost
     */
    virtual ~Ghost();

    /**
     * @brief eat
     * method to call for eat the componant if it s a pacman
     * @param c
     */
    virtual void eat(Component &c) override;

    /**
     * @brief scatterMode
     */
    bool scatterMode;

    /**
     * @brief fear
     */
    bool fear ;

    /**
     * @brief lastPos
     */
    Position lastPos;

    /**
     * @brief scatterPos
     */
    Position scatterPos;

    /**
     * @brief color
     * the color of the ghost
     */
    char color;

    /**
     * @brief init
     */
    bool init;

    /**
     * @brief getScatterMode
     * @return true if scatterMode is on
     */
    bool getScatterMode() const;

    /**
     * @brief setScatterMode
     * @param value
     */
    void setScatterMode(bool value);

    /**
     * @brief getScatterPos
     * @return the position of the corner where the ghost had to go on scatter mode
     */
    Position getScatterPos();

    /**
     * @brief getInit
     * @return true if the ghost is on initmode
     */
    bool getInit() const;

    /**
     * @brief setInit
     * @param value, the value to set the init of the ghost
     */
    void setInit(bool value);

    /**
     * @brief getFear
     * @return the state of fear
     */
    bool getFear() const;

    /**
     * @brief setFear
     * @param value
     */
    void setFear(bool value);
};

#endif // GHOST_H
