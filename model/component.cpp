#include "component.h"

using namespace std;


Component::Component()
{
    type =  MapComponent::EMPTY;
}

Component::Component(float x , float y, MapComponent type){
    this->pos = new Position(x,y);
    this->type = type ;
}

Component::Component(const Position &pos, MapComponent type)
{
    this->type = type;
    this->pos = new Position(pos);
    //this->pos = &pos;

}

Component::Component(const Component &cmpt)
{
    this->type = cmpt.getType();
    this->pos = new Position(cmpt.getPosition());
}

Component::~Component(){
    delete pos;
}

Position & Component::getPosition()const
{
    return *pos;
}

void Component::setPosition(const Position &pos)
{
   // this->pos->setX(pos.getX());
    //this->pos->setY(pos.getY());
    *(this->pos) = pos;
}



MapComponent Component::getType() const
{
    return  this->type;
}

void Component::setType(const MapComponent &value)
{
    type = value;
}

Component &Component::operator=(const Component &cmpt)
{
    if(this!=&cmpt){
        type = cmpt.type ;
        if(pos!=nullptr)
            delete pos ;
        this->pos = new Position(cmpt.getPosition());
    }
    return *this;
}
