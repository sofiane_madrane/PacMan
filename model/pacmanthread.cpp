#include "pacmanthread.h"
#include <QDebug>
using namespace std;


PacManThread::PacManThread(QObject *parent)
{
    game_ =(Game*) parent ;
}

PacManThread::~PacManThread()
{
}

void PacManThread::run()
{
    while(game_->getBegin() !=7); // 7 etat representant fin du son

    while(!game_->pac->isDead()){
        QMutex mutex;
        mutex.lock();
        game_->map_->moveCharacter(*game_->pac);
        game_->map_->hungry(*game_->pac);

        if(game_->map_->maybeAddFruits(game_->level) )
            emit emitFruitTimer();

        if( game_->pac->getMaybeBeastMode() )
            emit emitBeastModeTimer();

        emit emitNotify();
        mutex.unlock();
        usleep(30000);
    }

}

