#ifndef GBOARD_H
#define GBOARD_H
/*!
 *\file view/gboard.h
 */
#include <QMainWindow>
#include <QMessageBox>
#include <QObject>
#include <QPixmap>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <qdir>
#include <QApplication>
#include <QPushButton>
#include <QLabel>
#include <QSize>
#include <QComboBox>
#include <ctime>
#include <iostream>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsPixmapItem>
#include "launcher.h"
#include "o_sdo/observateur.h"
#include "model/game.h"

/*!
 * \class GBoard
 * \brief cette classe permet  de modéliser  le jeu Pac-Man. Afin d'afficher les objets du jeu à l'écran.
 *  Et de pouvoir y interagir. Elle respecte le  modèle de conception
 * "Observateur / SujetDObservation". Qui est englobé par MVC.
 * \author Madrane, Taranto, Annu, Keenens, Ordonnez
 * \version 0.1
 * \date 2017
 */

using namespace std;

class Launcher;

class GBoard : public QGraphicsView , public Observateur
{
    Q_OBJECT
public:
    /**
     * @brief GBoard Constructeur de la board du jeu.
     * @param jeu Model du jeu.
     * @param parent Fênetre d'application.
     */
    explicit GBoard(std::shared_ptr<Game>jeu, QWidget *parent=0);
    /**
     * @brief getStats
     * @return
     */
    QGraphicsView *getStats();
    /**
     *@brief Destructeur GBoard
     */
    ~GBoard();

public slots:
    /**
     * @brief animationGhostStart Fonction qui gére le début d'animations des
     * fantômes.
     */
    void animationGhostStart();
    /**
     * @brief animationGhostEnd Fonction qui mets fin à l'animations des
     * fantômes.
     */
    void animationGhostEnd();
    /**
     * @brief stopPrintText Fonction qui fait disparaître le texte d'acceuil du jeu.
     * "READY!"
     */
    void stopPrintText();
    /**
     * @brief startPrintScoreGhost Fonction qui adapte la vue avec le score apporté par
     * la consommation d'un fantôme.
     * @param value Score apporté par la consommation d'un fantôme.
     */
    void startPrintScoreGhost(int value);
    /**
     * @brief endPrintScoreGhost Fonction qui mets fin à l'adaptation de la vue au score apporté
     * par la consommation d'un fantôme.
     */
    void endPrintScoreGhost();

private:
    /**
     * @brief pacmanAnimation Fonction qui gére les animations de PacMan dans la vue.
     */
    void pacmanAnimation();
    /**
     * @brief initGhostAnimation Initialise les vues des fantômes.
     */
    void initGhostAnimation();
    /**
     * @brief ghostsAnimation Fonction qui gére les animations des ghosts dans la vue.
     */
    void ghostsAnimation();
    /**
     * @brief addItemBoard Fonction qui gére les ajouts d'item dans le board.
     */
    void addItemBoard();
    /**
     * @brief buildBoard Fonction qui crée la board dans laquelle se passe le jeu.
     */
    void buildBoard();
    /**
     * @brief buildStats Construits la barre de statistique joueur.
     */
    void buildStats();
    /**
     * @brief LifeBarRefresh Mets à jour la barre de vie du joueur.
     */
    void lifeBarRefresh();
    /**
     * @brief FruitBarRefresh Mets à jour la barre de niveau du jeu.
     */
    void fruitBarRefresh();
    /**
     * @brief imageGiver Donne l'image correspondant au niveau.
     * @param value Type de composant de la map.
     * @return Image correspondant au niveau.
     */
    QPixmap imageGiver(MapComponent value);

    /*!
     * \brief Cette méthode est appelée lorsque le sujet
     * observé notifie un changement.
     *
     * \param sdo : Le sujet d'observation qui notifie un changement.
     * \see SujetDObservation::notifierChangement().
     */
    void rafraichir(SujetDObservation * sdo);


private:
    /**
     * @brief scene_ Vue dans laquelle le jeu se passe.
     */
    QGraphicsScene scene_;
    /**
      * @brief scoring_ Vue texte du score.
      */
    QGraphicsTextItem*  scoring_ {nullptr};
    /**
      *@brief textGame Composant graphique texte du message d'acceuil ou de fin.
      */
    QGraphicsTextItem * textGame {nullptr};
    /**
     * @brief game_ Modéle du jeu.
     */
    shared_ptr<Game> game_;
    /**
     * @brief pacSprite_ Composant graphique du jeu.
     */
    QGraphicsPixmapItem pacSprite_;
    /**
     * @brief ghostsSprite Conteneur de composant graphique de fantôme.
     */
    vector<std::unique_ptr<QGraphicsPixmapItem>> ghostsSprite;
    /**
     * @brief mapSprite_ Image représentant la map.
     */
    QGraphicsPixmapItem mapSprite_;
    /**
     * @brief entitiesSprite_ Conteneur de tout les compasants graphiques se
     * trouvant dans la map.
     */
    vector<std::unique_ptr<QGraphicsPixmapItem>> entitiesSprite_;
    /**
     * @brief stats_ Statitisque joueur.
     */
    unique_ptr<QGraphicsView> stats_;
    /**
     * @brief lives_ Conteneur des composants graphiques du
     * nombre de vie.
     */
    vector<QGraphicsPixmapItem*> lives_;
    /**
     * @brief fruits_ Conteneur des composants graphiques des items
     * spéciaux.
     */
    vector<QGraphicsPixmapItem*> fruits_;
    /**
     * @brief fearGhostTimer Timer de la durée du mode peur des
     * fantômes
     */
    std::unique_ptr<QTimer> fearGhostTimer ;
    /**
     * @brief printTextTimer Timer de l'affichage du message d'acceuil.
     */
    std::unique_ptr<QTimer> printTextTimer ;

    /**
     * @brief printTextGhost Timer de l'affichage du score apporté par la consommation
     * d'un fantôme.
     */
    std::unique_ptr<QTimer> printTextGhost ;
    /**
     * @brief eatAnimation Etat de l'animation du Pac-Man.
     * Mangez ou pas.
     */
    bool eatAnimation ;
    /**
     * @brief animationGhost Etat de l'animation des fantômes.
     */
    bool animationGhost;


signals:
    /**
     * @brief passLevel Signal permettant de passer au niveau suivant.
     */
    void passLevel();
    /**
     * @brief restart Signal permettant de redémarrer le jeu.
     */
    void restart();
    /**
     * @brief oneMoreTime Signal notifiant la perte de vie du Pac-Man.
     */
    void oneMoreTime();

};

#endif // GBOARD_H
