#include "gboard.h"

GBoard::GBoard(shared_ptr<Game>jeu,QWidget *parent) :
    QGraphicsView(parent),
    scene_(this),
    game_(jeu),
    pacSprite_(QPixmap(":/pL1")),
    mapSprite_(QPixmap(":/map"))
{
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setGeometry(0, 0, parent->geometry().width(), parent->geometry().height());
    setBackgroundBrush(Qt::black);
    setScene(&scene_);
    eatAnimation = true;
    scene_.setSceneRect(0, 0, width(), height());


    //LES lignes
        for (int x=0; x< 700; x+=25)
            scene_.addLine(x,0,x,775, QPen(Qt::red));
        for (int y=0; y<=775; y+=25)
            scene_.addLine(0,y,700,y, QPen(Qt::red));





    setMinimumHeight(height());
    setMinimumWidth(width());
    setFrameStyle(QFrame::NoFrame);
    buildBoard();
    //Lance l'observateur
    game_->attacher(this);
    scoring_ =((Launcher*) parent)->getScoring();
    stats_ = make_unique<QGraphicsView>( new QGraphicsView(nullptr,((Launcher*) parent)));
    fearGhostTimer = make_unique<QTimer>(this);
    printTextTimer = make_unique<QTimer>(this);
    printTextGhost = make_unique<QTimer>(this);
    textGame =  new QGraphicsTextItem("READY!");

    initGhostAnimation();
    buildStats();
    fruitBarRefresh();
    animationGhost = false ;


    connect(fearGhostTimer.get(),SIGNAL(timeout()),this,SLOT(animationGhostEnd()));
    connect(game_.get(),SIGNAL(emitFear()),this,SLOT(animationGhostStart()));
    connect(printTextTimer.get(),SIGNAL(timeout()),this,SLOT(stopPrintText()));
    connect(game_->getPlayer().get(),&Pacman::scoreGhost,this,&GBoard::startPrintScoreGhost);
    connect(printTextGhost.get(),SIGNAL(timeout()),this,SLOT(endPrintScoreGhost()));


    textGame->setFont(game_->getpacFont());
    textGame->setDefaultTextColor(Qt::yellow);
    textGame->setTransform(QTransform().translate(280,410));
    scene_.addItem(textGame);
    printTextTimer->start(4000);

}

void GBoard::rafraichir(SujetDObservation *sdo)
{

    if (sdo != game_.get()) return;

   //rafraichis la barre de score et de vie
    scoring_->setPlainText(QString::number(game_->getPlayer()->getScore()));
    lifeBarRefresh();

    //rafraichir board
    if (!game_->getPlayer()->getAlive()){
        emit oneMoreTime(); //notifie le controller
    }

    if(game_->getPlayer()->isDead()) {
        textGame->setPlainText("GAME OVER");
        textGame->setTransform(QTransform().translate(240,410));
        textGame->setDefaultTextColor(Qt::red);
        int reponse =  QMessageBox::question(this, "Nouveau jeu", "Voulez-vous refaire une partie ?",
                                             QMessageBox ::Yes | QMessageBox::No);

        if (reponse == QMessageBox::Yes) {
            textGame->setPlainText("READY!");
            textGame->setDefaultTextColor(Qt::yellow);
            textGame->setTransform(QTransform().translate(280,410));
            printTextTimer->start(2000);
            emit restart(); // J'avertis le controller de redemarrer la game
        }else
            exit(0);
    }
    if(game_->maybeNextLevel()){
        emit passLevel(); // J'avertis le controller de passer le niveau
        fruitBarRefresh();
    }

    entitiesSprite_.clear();
    addItemBoard();
    pacmanAnimation();
    ghostsAnimation();

    scene_.update();
}


void GBoard::pacmanAnimation()
{
    QPixmap pix ;

    switch (game_->getPlayer()->getDirection()){
        case Direction::LEFT:
           if(eatAnimation){
               pix = QPixmap(":/pL1");
               eatAnimation = false;
           }else{
               eatAnimation = true;
               pix = QPixmap(":/pL2");
            }
            break;
        case Direction::RIGHT:
            if(eatAnimation){
                eatAnimation = false ;
                pix = QPixmap(":/pR1");
            }else{
                eatAnimation = true ;
                pix = QPixmap(":/pR2");
             }
             break;

        case Direction::UP:
            if(eatAnimation) {
                eatAnimation = false;
                pix = QPixmap(":/pU1");
            }else{
                eatAnimation = true ;
                pix = QPixmap(":/pU2");
            }
            break;
        case Direction::DOWN:
            if(eatAnimation) {
                eatAnimation = false;
                pix = QPixmap(":/pD1");
            }else {
                eatAnimation = true;
                pix = QPixmap(":/pD2");
            }
            break;
    }
    pacSprite_.setPixmap(pix);

    pacSprite_.setTransform(QTransform().translate(
        game_->getPlayer()->getPosition().getX(),
        game_->getPlayer()->getPosition().getY()));
}

void GBoard::initGhostAnimation()
{
    for (unsigned i = 0; i<game_->getGhosts().size();i++) {
        //Ghost ghost = *jeu->getGhosts()[i];
        auto ghost = game_->getGhosts()[i];
        switch (ghost->getType()) {
            case MapComponent::G_RED:
            {
                auto item = std::make_unique<QGraphicsPixmapItem>(QPixmap(":/gR"));
                item->setTransform(QTransform().translate(
                    ghost->getPosition().getX(),
                    ghost->getPosition().getY()
                ));

                scene_.addItem(item.get());
                ghostsSprite.push_back(std::move(item));
                break;
            }
            case MapComponent::G_BLUE:
            {
                auto item = std::make_unique<QGraphicsPixmapItem>(QPixmap(":/gB"));
                item->setTransform(QTransform().translate(
                    ghost->getPosition().getX(),
                    ghost->getPosition().getY()
                ));
                scene_.addItem(item.get());
                ghostsSprite.push_back(std::move(item));
                break;
            }
            case MapComponent::G_ORANGE:
            {
                auto item = std::make_unique<QGraphicsPixmapItem>(QPixmap(":/gO"));
                item->setTransform(QTransform().translate(
                    ghost->getPosition().getX(),
                    ghost->getPosition().getY()
                ));

                scene_.addItem(item.get());
                ghostsSprite.push_back(std::move(item));
                break;
            }
            case MapComponent::G_PINK:
            {
                auto item = std::make_unique<QGraphicsPixmapItem>(QPixmap(":/gP"));
                item->setTransform(QTransform().translate(
                   ghost->getPosition().getX(),
                   ghost->getPosition().getY()
                ));

                scene_.addItem(item.get());
                ghostsSprite.push_back(std::move(item));
                break;
            }
        default: break;
        }
    }
}

void GBoard::ghostsAnimation()
{
    QPixmap pix ;

    for (unsigned i = 0; i<game_->getGhosts().size();i++) {
        auto ghost = game_->getGhosts()[i];
        switch (ghost->getType()) {
            case MapComponent::G_RED:
            {
                if (game_->getPlayer()->getBeastMode() &&
                        ghost->getScatterMode()) {
                    if(eatAnimation || animationGhost) {
                        //eatAnimation = false ;
                         pix = QPixmap(":/gp1");
                    }else{
                        //eatAnimation = true ;
                        pix = QPixmap (":/gP2");
                    }

                } else
                    pix = QPixmap(":/gR");
                break;
            }
            case MapComponent::G_BLUE:
            {
                if (game_->getPlayer()->getBeastMode() &&
                        ghost->getScatterMode()) {
                    if(eatAnimation || animationGhost) {
                        //eatAnimation = false ;
                         pix = QPixmap(":/gp1");
                    }else{
                        //eatAnimation = true ;
                        pix = QPixmap (":/gP2");
                    }

                } else
                    pix = QPixmap(":/gB");

                break;
            }
            case MapComponent::G_ORANGE:
            {
                if (game_->getPlayer()->getBeastMode() &&
                        ghost->getScatterMode()) {

                    if(eatAnimation || animationGhost) {
                        //eatAnimation = false ;
                         pix = QPixmap(":/gp1");
                    }else{
                        //eatAnimation = true ;
                        pix = QPixmap (":/gP2");
                    }

                } else
                    pix = QPixmap(":/gO");

                break;
            }
            case MapComponent::G_PINK:
            {
                if (game_->getPlayer()->getBeastMode() &&
                        ghost->getScatterMode()) {
                    if(eatAnimation || animationGhost) {
                        //eatAnimation = false ;
                         pix = QPixmap(":/gp1");
                    }else{
                        //eatAnimation = true ;
                        pix = QPixmap (":/gP2");
                    }

                } else
                    pix = QPixmap(":/gP");

                break;
            }
        default : break;
        }
        ghostsSprite[i]->setPixmap(pix);

        ghostsSprite[i]->setTransform(QTransform().translate(
                                     ghost->getPosition().getX(),
                                     ghost->getPosition().getY()));
    }
    if(!animationGhost)
        fearGhostTimer->stop();
}

void GBoard::addItemBoard()
{
    auto& map = game_->getMapGame()->getMap();
    for (auto& line : map) {
        for (auto& compo : line) {
            std::unique_ptr<QGraphicsPixmapItem> item;
            switch (compo.getType()) {
                case MapComponent::BISCUIT:
                {
                    auto item = std::make_unique<QGraphicsPixmapItem>(QPixmap(":/dot"));
                    item->setTransform(QTransform().translate(
                        compo.getPosition().getX(),
                        compo.getPosition().getY()
                    ));
                    scene_.addItem(item.get());
                    entitiesSprite_.push_back(std::move(item));
                    break;
                }
                case MapComponent::FLASH_BISCUIT:
                {
                    auto item = std::make_unique<QGraphicsPixmapItem>(QPixmap(":/flashdot"));
                    item->setTransform(QTransform().translate(
                        compo.getPosition().getX(),
                        compo.getPosition().getY()
                    ));

                    scene_.addItem(item.get());
                    entitiesSprite_.push_back(std::move(item));
                    break;
                }
                case MapComponent::USE:
                case MapComponent::EMPTY:
                {
                    auto item = std::make_unique<QGraphicsPixmapItem>(QPixmap());
                    item->setTransform(QTransform().translate(
                        compo.getPosition().getX(),
                        compo.getPosition().getY()
                    ));
                    scene_.addItem(item.get());
                    entitiesSprite_.push_back(std::move(item));
                    break;
                }
                case MapComponent::CHERRY:
                {
                    auto item = std::make_unique<QGraphicsPixmapItem>(QPixmap(":/cherry"));
                    item->setTransform(QTransform().translate(
                        compo.getPosition().getX(),
                        compo.getPosition().getY()
                    ));
                    scene_.addItem(item.get());
                    entitiesSprite_.push_back(std::move(item));
                    break;;
                }
                case MapComponent::APPLE:
                {
                    auto item = std::make_unique<QGraphicsPixmapItem>(QPixmap(":/apple"));
                    item->setTransform(QTransform().translate(
                        compo.getPosition().getX(),
                        compo.getPosition().getY()
                    ));
                    scene_.addItem(item.get());
                    entitiesSprite_.push_back(std::move(item));
                    break;
                }
                case MapComponent::PINEAPPLE:
                {
                    auto item = std::make_unique<QGraphicsPixmapItem>(QPixmap(":/pineaplle"));
                    item->setTransform(QTransform().translate(
                        compo.getPosition().getX(),
                        compo.getPosition().getY()
                    ));
                    scene_.addItem(item.get());
                    entitiesSprite_.push_back(std::move(item));
                    break;
                }
                case MapComponent::STRAWBERRY:
                {
                    auto item = std::make_unique<QGraphicsPixmapItem>(QPixmap(":/strawberry"));
                    item->setTransform(QTransform().translate(
                        compo.getPosition().getX(),
                        compo.getPosition().getY()
                    ));
                    scene_.addItem(item.get());
                    entitiesSprite_.push_back(std::move(item));
                    break;
                }
                case MapComponent::ORANGE:
                {
                    auto item = std::make_unique<QGraphicsPixmapItem>(QPixmap(":/orange"));
                    item->setTransform(QTransform().translate(
                        compo.getPosition().getX(),
                        compo.getPosition().getY()
                    ));
                    scene_.addItem(item.get());
                    entitiesSprite_.push_back(std::move(item));
                    break;
                }
                case MapComponent::GALAXIAN:
                {
                    auto item = std::make_unique<QGraphicsPixmapItem>(QPixmap(":/galaxian"));
                    item->setTransform(QTransform().translate(
                        compo.getPosition().getX(),
                        compo.getPosition().getY()
                    ));
                    scene_.addItem(item.get());
                    entitiesSprite_.push_back(std::move(item));
                    break;
                }
                case MapComponent::KEY:
                {
                    auto item = std::make_unique<QGraphicsPixmapItem>(QPixmap(":/key"));
                    item->setTransform(QTransform().translate(
                        compo.getPosition().getX(),
                        compo.getPosition().getY()
                    ));
                    scene_.addItem(item.get());
                    entitiesSprite_.push_back(std::move(item));
                    break;
                }
            default: break;
            }
        }
    }
}

QPixmap GBoard::imageGiver(MapComponent value){
    QPixmap pix ;

    switch(value){
    case MapComponent::BISCUIT: pix = QPixmap(":/dot"); break;
    case MapComponent::FLASH_BISCUIT: pix = QPixmap(":/flashdot");break;
    case MapComponent::G_RED: pix = QPixmap(":/gR");break;
    case MapComponent::G_BLUE: pix = QPixmap(":/gB");break;
    case MapComponent::G_ORANGE: pix = QPixmap(":/gO");break;
    case MapComponent::G_PINK: pix = QPixmap(":/gP");break;
    case MapComponent::USE:
    case MapComponent::EMPTY: pix = QPixmap();break;
    case MapComponent::CHERRY: pix = QPixmap(":/cherry");break;
    case MapComponent::APPLE: pix = QPixmap(":/apple");break;
    case MapComponent::PINEAPPLE: pix = QPixmap(":/pineaplle");break;
    case MapComponent::STRAWBERRY: pix = QPixmap(":/strawberry");break;
    case MapComponent::ORANGE: pix = QPixmap(":/orange");break;
    case MapComponent::GALAXIAN: pix = QPixmap(":/galaxian");break;
    case MapComponent::KEY: pix = QPixmap(":/key");break;
    default : break;
    }

    return pix;
}

void GBoard::buildBoard()
{

    //Utilitaire pour quadriller la carte et afficher les murs.

    /*//LES lignes
    for (int x=0; x< 700; x+=25)
        scene_.addLine(x,0,x,775, QPen(Qt::red));
    for (int y=0; y<=775; y+=25)
        scene_.addLine(0,y,700,y, QPen(Qt::red));
    //scene_.itemsBoundingRect();
    for (int x=0; x< 29; x++){
             for (int y=0; y<31; y++){
                if(game_.get()->getMapGame()->getMap()[y][x].getType() == MapComponent::WALL){
                    //scene_.addRect(x*25, y*25, 25, 25, QPen(Qt::blue), QBrush(Qt::blue));
                }
             }
        }*/
    scene_.addItem(&mapSprite_);

    // Ajouter les dots et les ghosts
    addItemBoard();

    // Ajouter pacman
    pacSprite_.setTransform(QTransform().translate(
        game_->getPlayer()->getPosition().getX(),
        game_->getPlayer()->getPosition().getY()
    ));

    scene_.addItem(&pacSprite_);
}
void GBoard::buildStats(){

    stats_->setScene(new QGraphicsScene);
    stats_->setBackgroundBrush(Qt::black);
    stats_->setAlignment(Qt::AlignTop|Qt::AlignLeft);
    stats_->setSceneRect(0,0,50,50);
    stats_->setFrameStyle(QFrame::NoFrame);

    //5 vies maximum
    QGraphicsPixmapItem *tmp;
        for(int i =0; i<=5;i++){
           tmp = new QGraphicsPixmapItem(QPixmap(":/pL1"));
           tmp->setTransform(QTransform().translate(50+(45*i),0).scale(1.5,1.5));


           lives_.push_back(tmp);
        }
    lifeBarRefresh();
}

void GBoard::lifeBarRefresh(){
    QGraphicsPixmapItem *tmp;
    int lives = game_->getPlayer()->getLives();
    for(int i=0;i<5;i++){
        tmp = lives_.at(i);
        if(i<lives-1){
            if(tmp->scene() != stats_->scene()){
                stats_->scene()->addItem(tmp);
            }
        }else{
            if(tmp->scene() == stats_->scene()){
                stats_->scene()->removeItem(tmp);
            }
        }
    }
}

void GBoard::fruitBarRefresh(){
    for (unsigned i =0; i< fruits_.size();i++){
        stats_->scene()->removeItem(fruits_[i]);
         delete (fruits_[i]);
       }
    fruits_.clear();

    int minLevel;
    if(game_->getLevel()>7){
        minLevel=game_->getLevel()-6;
    }else{
        minLevel =1;
    }
     QGraphicsPixmapItem *tmp;
    for(unsigned i=2;  minLevel<=game_->getLevel(); minLevel++,i++){
         tmp=new QGraphicsPixmapItem(imageGiver(game_->getMapGame()->SpecialItemGiver(minLevel)));
         tmp->setTransform(QTransform().translate(width()-10-(50*i),0).scale(1.5,1.5));
        fruits_.push_back(tmp);
        stats_->scene()->addItem(tmp);
    }
}




QGraphicsView *GBoard::getStats(){
    return stats_.get();
}

GBoard::~GBoard(){
    if(textGame != nullptr)
        delete textGame ;

    for (unsigned i =0; i< lives_.size();i++)
       {
         delete (lives_[i]);
       }
    lives_.clear();

    this->game_->detacher(this);
}

void GBoard::animationGhostStart()
{
    animationGhost = true ;
    fearGhostTimer->start(7000);
}

void GBoard::animationGhostEnd()
{
    animationGhost = false;
    //fearGhost->stop();
}

void GBoard::stopPrintText()
{
    textGame->setPlainText(" ");
    printTextTimer->stop();
}

void GBoard::startPrintScoreGhost(int value)
{
    textGame->setPlainText(QString::number(value));
    textGame->setTransform(QTransform().translate( game_->getPlayer()->getPosition().getX(),
                                                   game_->getPlayer()->getPosition().getY()));
    textGame->setDefaultTextColor(Qt::yellow);
    printTextGhost->start(1000);
}

void GBoard::endPrintScoreGhost()
{
    printTextGhost->stop();
    textGame->setPlainText(" ");
}




