#ifndef LAUNCHER_H
#define LAUNCHER_H
/*!
 *\file view/launcher.h
 *
 */
#include <QMainWindow>
#include <QApplication>
#include <QTime>
#include <QtGui>
#include "gboard.h"
#include "model/game.h"
#include "controller/controller.h"
/*!
 * \class MainStarlight
 * \brief Cette classe génère le programme et s'occupe du bon lancement de la partie.
 * \author Madrane Sofiane
 * \author ...
 * \version 0.1
 * \date 2017
 */

using namespace std;

class GBoard;
class Controller;

namespace Ui {
class Launcher;
}

class Launcher : public QMainWindow
{
    Q_OBJECT

public:
    /*!
     * \brief Constructeur de la classe Launcher.
     * \param sdo : Partie qui est sujet d'observation
     * \param parent : Vue de la fenêtre parent.
     */
    explicit Launcher(QWidget *parent = 0);
    /*!
     * \brief Destructeur de la classe Launcher.
     */
    ~Launcher();
    /**
     * @brief getScoring Accesseur composant graphique du score.
     * @return  Pointeur composant graphique du score.
     */
    QGraphicsTextItem *getScoring();

protected:
    //void resizeEvent(QResizeEvent *event); //pas utile
    /**
     * @brief timerEvent boucle principale du jeu
     * @param event
     */
    void timerEvent(QTimerEvent *event);//pas utileS
    /*!
     * \brief keyPressEvent
     * Permet d'affecter une action à certaines touches du clavier.
     * \param event : Événement d'une touche du clavier.
     */
    void keyPressEvent(QKeyEvent *event);



private:
    /**
      * @brief corps_ Conteneur des trois vues.
      */
    QVBoxLayout* corps_ {nullptr};
    /**
     * @brief game_ La jeu en cours.
     */
    shared_ptr<Game> game_;
    /**
     * @brief gboard_ Vue du jeu Pac-Man.
     */
    unique_ptr<GBoard> gboard_;
    /**
     * \brief scores_ Composant graphique du score.
     */
    unique_ptr<QGraphicsView> scores_;
    /**
      *@brief controller_ Controlleur du jeu Pac-Man.(IHM)
      */
    Controller* controller_ {nullptr};
    /**
      *@brief scoring_ Vue texte du score.
      */
    QGraphicsTextItem *scoring_ {nullptr};
    /*!
     * \brief ui Fênetre mère du programme.
     */
    Ui::Launcher *ui;
    /**
     * @brief start Fonction qui initialise le Launcher.
     */
    void start();   
    /**
     * @brief buildScore Fonction qui initialise le composant graphique du score.
     */
    void buildScore();


    QTime time_;  //pas utile
    int oldTimeMS_;//pas utile

signals:
    /**
     * @brief move Signal qui va permettre de notifier au controller
     * qu'une touche a été pressée.
     * @param dir Direction , en fonction de la touche pressée.
     */
    void move(Direction dir);

};

#endif // LAUNCHER_H
