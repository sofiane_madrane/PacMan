#include "launcher.h"
#include "ui_launcher.h"
#include <QApplication>
#include <QResizeEvent>
#include <iostream>
#include <QDir>
#include <conio.h>

Launcher::Launcher(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Launcher),
    oldTimeMS_(0)
{
    setStyleSheet("QMainWindow {background: 'black';}");
    ui->setupUi(this);
    connect(ui->lanceur, &QPushButton::clicked, this, &Launcher::start);
    scoring_= new QGraphicsTextItem("00");
}

Launcher::~Launcher()
{  
    if(corps_ !=nullptr){
        delete corps_;
    }
    if (controller_ != nullptr)
        delete controller_;

    delete ui;
}

QGraphicsTextItem *Launcher::getScoring()
{
    return scoring_;
}

void Launcher::start()
{
    ui->lanceur->hide(); // hides the launch button
    game_ = std::make_shared<Game>();
    gboard_ = std::make_unique<GBoard>(game_, this); //Le gboard est un GraphicsView par héritage
    controller_ = new Controller (game_) ;
    setFocus();

    corps_ = new QVBoxLayout;

    //Signaux poour le controller afin de modifier le modele
    // MVC
    connect(this,&Launcher::move,controller_,&Controller::pacmanMove);
    connect(gboard_.get(),&GBoard::passLevel,controller_,&Controller::passNextLevel);
    connect(gboard_.get(),&GBoard::restart,controller_,&Controller::restart);
    connect(gboard_.get(),&GBoard::oneMoreTime,controller_,&Controller::oneMorePacman);

    buildScore();
    corps_->addWidget(scores_.get());
    corps_->addWidget(gboard_.get());
    corps_->addWidget(gboard_.get()->getStats());
    QWidget *window = new QWidget();
    window->setLayout(corps_);
    setCentralWidget(window);
    //
    //time_.start();
    startTimer(16); // 16ms = 62.5 fps
}

void Launcher::buildScore()
{
    scores_ = std::make_unique<QGraphicsView>(new QGraphicsView(nullptr, this));
    scores_.get()->setBackgroundBrush(Qt::black);
    scores_.get()->setSceneRect(0,0,50,50);
    scores_.get()->setFrameStyle(QFrame::NoFrame);
    scores_->setAlignment(Qt::AlignTop|Qt::AlignLeft);
    scores_.get()->setScene(new QGraphicsScene);

    QGraphicsTextItem *highScore =  new QGraphicsTextItem("1UP   High Score");
    highScore->setFont(game_->getpacFont());
    highScore->setDefaultTextColor(Qt::gray);
    //highScore
    highScore->setTransform(QTransform().translate(75,0));
    scores_->scene()->addItem(highScore);


    scoring_->setFont(game_->getpacFont());
    scoring_->setDefaultTextColor(Qt::gray);
    scoring_->setTransform(QTransform().translate(125,25));
    scores_.get()->scene()->addItem(scoring_);
}





void Launcher::keyPressEvent(QKeyEvent *event)
{
    Direction direction ;
    switch(event->key())
    {
        case Qt::Key_Down:
        case Qt::Key_S:
            direction = Direction::DOWN;
            break;

        case Qt::Key_Up:
        case Qt::Key_Z:
            direction = Direction::UP ;
            break;

        case Qt::Key_Left:
        case Qt::Key_Q:
            direction = Direction::LEFT ;
            break;

        case Qt::Key_Right:
        case Qt::Key_D:
            direction = Direction::RIGHT ;
            break;
        default: direction = Direction::LEFT;
            break;
    }
    emit move(direction);
}

void Launcher::timerEvent(QTimerEvent *event)
{
    // position += velocity * dt / 1000.0; velocity = speed * direction;
    oldTimeMS_ = time_.elapsed();
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Launcher l;
    l.show();

    return a.exec();
}
