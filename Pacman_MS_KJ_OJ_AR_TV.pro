

#-------------------------------------------------
#
# Project created by QtCreator 2016-11-15T14:06:29
#
#-------------------------------------------------

QT       += widgets core gui multimedia

CONFIG += console c++14
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Pacman_MS_KJ_OJ_AR_TV
TEMPLATE = app

QMAKE_CXXFLAGS += -O0

SOURCES += model/game.cpp \
    model/pacman.cpp \
    model/ghost.cpp \
    model/character.cpp \
    model/position.cpp \
    model/component.cpp \
    model/mapgame.cpp \
    view/launcher.cpp \
    model/o_sdo/sujetdobservation.cpp \
    model/pacmanthread.cpp \
    model/ghoststhread.cpp \
    controller/controller.cpp \
    view/gboard.cpp

HEADERS  +=  \
    model/game.h \
    model/pacman.h \
    model/ghost.h \
    model/character.h \
    model/direction.h \
    model/position.h \
    model/component.h \
    model/mapcomponent.h \
    model/mapgame.h \
    view/launcher.h \
    model/o_sdo/observateur.h \
    model/o_sdo/sujetdobservation.h \
    model/pacmanthread.h \
    model/ghoststhread.h \
    controller/controller.h \
    view/gboard.h


FORMS    += \
    launcher.ui

RESOURCES += \
    ressources/src.qrc
