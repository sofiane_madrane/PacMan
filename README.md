
# C++ PACMAN

Le projet représente un jeu le plus fidèle possible du classique jeu de PacMan, nous utilisons principalement les librairies de Qt, pour la gestion de thread, multimédia et interface graphique c’est le cas des composants graphiques QGraphicsItems.

## MATERIEL INFORMATIQUE

Voici le programmes ou services utilisées pour le projet avec une brève description.

- QT
- GitLab
- GitKraken

## DESCRIPTION DES CLASSES

- Component : Cette classe représente tous les éléments trouvant sur la map dans le jeu Pac-Man.
- Character : Définis les entités, tel le joueur(Pac-Man) et les fantômes. Dans le jeu Pac-Man. Elle gère les caractéristiques de base des personnages.
- Pacman : Représente le joueur(Pac-Man) dans le jeu Pac-Man.
- Ghost : Représente le fantôme dans le jeu Pac-Man.
- PacmanThread : Thread Pac-Man s’occupant du déplacement.
- GhostThread : Thread fantôme s’occupant du déplacement de celui-ci.
- Game : Cette classe permet de gérer le jeu Pac-Man. Elle gère toutes les caractéristiques et détails du jeu. Elle respecte le modèle de conception "Observateur / SujetDObservation». Elle est sujet d'observation.
- MapGame : Cette classe s’occupe de tout ce qui est vérification de collision et de déplacement. Ainsi que les IA des fantômes.
- Direction : Enumération des différents types de direction possible.
- MapComponent : Enumération des types composants de la map.
- Position : Cette classe permet de modéliser une position dans le jeu Pac-Man.
- Controller : Cette classe permet de contrôler le jeu Pac-Man. Afin de pouvoir y interagir avec. Elle respecte modèle de conception "MVC".
- GBoard : Cette classe permet de modéliser le jeu Pac-Man. Afin d'afficher les objets du jeu à l'écran. Et de pouvoir y interagir. Elle respecte le modèle de conception "Observateur / SujetDObservation". Qui est englobé par MVC.
- Launcher : Cette classe génère le programme et s'occupe du bon lancement de la partie.


## DESCRIPTION DU TRAVAIL RÉALISÉ
|           |           |
| :-------: | :------- |
| Semaine 1 | Choix du projet |
| Semaine 2 | Analyse complète du projet Pacman avec classes et méthodes a créé. |
| Semaine 3 | Réalisation des premières classes importante pour le jeu. |
| Semaine 4 | Mise en place d'un design sous QT. |
| Semaine 5 | Gestion des déplacements avec collision. |
| Semaine 6 | Correction de multiples erreurs et bugs survenu lors des déplacements.Analyse et preétude des IA pour les fantômes. | 
| Semaine 7 | Mise en place d'un thread permettant une gestion de Pacman. Implémentation de l’IA pour le fantôme rouge (Car celui-ci modifie sa vitesse de déplacement au fils de temps) |
| Semaine 8 | Mise en commun des IA et des déplacements avec énormément de Tests et d'adaptation pour que les codes concordent. Résolution de conflits et tests. |

### Répartition du travail

- Madrane Sofiane
   - Gestion MVC
   - Gestion des entités (se déplacer, manger)
   - Gestion du Modèle
   - Gestion de la vue
- Taranto Valentin
    - Gestion de la vue
    - Gestion du son
    - Gestion de la map
- Keenens Jordan
    - Testeur, débogueur
    - Gestion de la map
    - Gestion des entités
- Annu Raphaël
    - Gestion des IA ennemis
- Ordonez Jonathan
    - Gestion des IA ennemis

## Fabriqué avec


* [QT](https://www.qt.io/) - QT IDE (C++ 11)
* [GitKraken](https://www.gitkraken.com/) - Git GUI
* [Trello](https://www.trello.com/)

## Conclusion

Ce travail de groupe nous a permis de réaliser à quel point sans organisation, un travail de groupe est très compliqué. C’est pour cela qu’il est important de se donner des tâcher, ainsi d’utiliser la bonne méthodologie vue aux cours d’analyse. Ce projet nous a permis d’appliquer toute les connaissances vues durant notre bachelier. De la logique, l’analyse et la programmation. Ainsi que d’apprendre par nous-même et d’être d’autodidacte. Car cela nous a permis de nous familiariser avec l’environnement Qt. Ainsi que les nouveautés de C++, comme les smart ptr.


## Auteurs

* **Sofiane Madrane** _alias_ [@sofiane_madrane](https://gitlab.com/sofiane_madrane)
* **Taranto Valentin**
* **Keenens Jordan**
* **Annu Raphaël**
* **Ordonez Jonathan**

## License

Ce projet est sous licence ``exemple: WTFTPL`` - voir le fichier [LICENSE.md](LICENSE.md) pour plus d'informations
